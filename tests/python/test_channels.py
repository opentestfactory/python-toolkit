# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Expressions unit tests."""

import json
import logging
import unittest
import re

from unittest.mock import MagicMock, mock_open, patch

from opentf.toolkit import channels


########################################################################

COMMAND_TEARDOWN = {
    'metadata': {
        'step_sequence_id': -2,
        'job_id': 'jOb_iD',
    },
    'scripts': [],
}

COMMAND_0_NOHOOKS = {
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'step_sequence_id': 0,
        'job_id': 'jOb_iD',
        'channel_os': 'linux',
        'channel_temp': '/tmp',
        'step_id': 'id',
    },
    'scripts': [],
}

COMMAND_0_WD_NOHOOKS = {
    'metadata': {
        'step_sequence_id': 0,
        'job_id': 'jOb_iD',
    },
    'scripts': [],
    'working-directory': 'wORking/DireCtory',
}

COMMAND_0_HOOKS_BEFORE = {
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'step_sequence_id': 0,
        'job_id': 'jOb_iD',
        'channel_os': 'linux',
        'annotations': {'opentestfactory.org/hooks': {'channel': 'setup'}},
    },
    'scripts': [],
}

COMMAND_0_HOOKS_AFTER = {
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'step_sequence_id': 0,
        'job_id': 'jOb_iD',
        'channel_os': 'linux',
        'annotations': {
            'opentestfactory.org/hooks': {
                'channel': 'setup',
                'use-workspace': 'worKspAce',
            }
        },
    },
    'scripts': [],
}

COMMAND_1 = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
    },
    'scripts': [],
}
COMMAND_1_WD = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
    },
    'scripts': [],
    'working-directory': 'wORking/DireCtory',
}
COMMAND_1_WD_SPACES = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
    },
    'scripts': [],
    'working-directory': 'wORk ing/DireCtory',
}
COMMAND_1_VAR = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
    },
    'variables': {'vAr': 'vALuE'},
    'scripts': [],
}

COMMAND_1_LOCAL_VAR = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
        'annotations': {'local_variables_names': ['vAr']},
    },
    'variables': {'vAr': 'vALuE', 'vAr2': 'value'},
    'scripts': [],
}

COMMAND_1_VAR_WD = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
    },
    'variables': {'vAr': 'vALuE'},
    'scripts': [],
    'working-directory': 'wORking/DireCtory',
}

COMMAND_SHELL_VAR_WD = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
    },
    'variables': {'vAr': 'vALuE'},
    'scripts': ['foo'],
    'shell': 'python',
    'working-directory': 'wORking/DireCtory',
}

COMMAND_1_ARTIFACTS = {
    'metadata': {
        'step_sequence_id': 1,
        'job_id': 'jOb_iD',
        'artifacts': ['/tmp/123_WR_1.txt', '/tmp/456_WR_2.txt'],
        'channel_os': 'linux',
        'channel_temp': '/tmp',
    },
    'scripts': [],
}

COMMAND_TEARDOWN_HOOKS_KEEP = {
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'step_sequence_id': -2,
        'job_id': 'jOb_iD',
        'channel_os': 'linux',
        'annotations': {
            'opentestfactory.org/hooks': {
                'channel': 'teardown',
                'keep-workspace': True,
            }
        },
    },
    'scripts': [],
}

COMMAND_TEARDOWN_HOOKS_DONOTKEEP = {
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'step_sequence_id': -2,
        'job_id': 'jOb_iD',
        'channel_os': 'linux',
        'annotations': {
            'opentestfactory.org/hooks': {
                'channel': 'teardown',
                'keep-workspace': False,
            }
        },
    },
    'scripts': [],
}

RESULT_UPLOAD_ONLY = {
    'attachments': ['/tmp/wfid_att1'],
    'metadata': {
        'upload': 0,
        'workflow_id': 'wfid',
        'job_id': 'jobid',
        'step_id': 'uploadartifact-name-uuid',
        'namespace': 'default',
        'attachments': {'/tmp/wfid_att1': {'uuid': 'uuid1'}},
    },
}

RESULT_UPLOAD_ATTACH = {
    'attachments': ['/tmp/wfid_att1', '/tmp/jobid_att2'],
    'metadata': {
        'upload': 0,
        'workflow_id': 'wfid',
        'job_id': 'jobid',
        'step_id': 'uuid',
        'namespace': 'default',
        'attachments': {
            '/tmp/wfid_att1': {'uuid': 'uuid1'},
            '/tmp/jobid_att2': {'uuid': 'uuid2'},
        },
    },
}

RESULT_VARS = {
    'attachments': ['/tmp/dynamic_env.sh'],
    'metadata': {
        'workflow_id': 'wfid',
        'job_id': 'jobid',
        'step_id': 'uuid',
        'namespace': 'default',
        'attachments': {
            '/tmp/dynamic_env.sh': {
                'uuid': 'uuid1',
                'type': 'application/vnd.opentestfactory.opentf-variables',
            },
        },
    },
}

RESULT_VARS_ATTACH = {
    'attachments': ['/tmp/dynamic_env.sh', '/tmp/jobid_att2'],
    'metadata': {
        'workflow_id': 'wfid',
        'job_id': 'jobid',
        'step_id': 'uuid',
        'namespace': 'default',
        'attachments': {
            '/tmp/dynamic_env.sh': {
                'uuid': 'uuid1',
                'type': 'application/vnd.opentestfactory.opentf-variables',
            },
            '/tmp/jobid_att2': {'uuid': 'uuid2'},
        },
    },
}


UUID_REGEX = r'^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$'
########################################################################


class TestChannels(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # make_variable_*

    def test_mvl_1(self):
        self.assertEqual(channels.make_variable_linux('VAR', 'fOo'), 'export VAR=fOo')

    def test_mvl_2(self):
        self.assertEqual(
            channels.make_variable_linux('VAR', 'fOo bAr'), 'export VAR="fOo bAr"'
        )

    def test_mvl_3(self):
        self.assertEqual(
            channels.make_variable_linux('VAR', 'fOo"bAr'), 'export VAR=fOo\\"bAr'
        )

    def test_mvl_v(self):
        self.assertEqual(
            channels.make_variable_linux('VAR', {'value': 'fOo"bAr', 'verbatim': True}),
            'export VAR=\'fOo"bAr\'',
        )

    def test_mvw_1(self):
        self.assertEqual(channels.make_variable_windows('VAR', 'fOo'), '@set "VAR=fOo"')

    def test_mvw_2(self):
        self.assertEqual(
            channels.make_variable_windows('VAR', 'fOo bAr'), '@set "VAR=fOo bAr"'
        )

    def test_mvw_v(self):
        self.assertEqual(
            channels.make_variable_windows(
                'VAR', {'value': 'fOo^bAr', 'verbatim': True}
            ),
            '@set "VAR=fOo^^bAr"',
        )

    # make_script

    def test_ms_minus2(self):
        path, script, command = channels.make_script(COMMAND_TEARDOWN, None, 'windows')
        self.assertIn('@rmdir', script)
        self.assertTrue(path.startswith(channels.SCRIPTPATH_DEFAULT['windows']))

    def test_ms_minus2_linux(self):
        path, script, command = channels.make_script(COMMAND_TEARDOWN, None, 'linux')
        self.assertIn('rm -rf', script)
        self.assertTrue(path.startswith(channels.SCRIPTPATH_DEFAULT['linux']))

    def test_ms_firststep_afterhook_linux(self):
        path, script, command = channels.make_script(
            COMMAND_0_HOOKS_AFTER, None, 'linux'
        )
        self.assertIn('touch', script)
        self.assertTrue(path.startswith(channels.SCRIPTPATH_DEFAULT['linux']))

    def test_ms_firststep_afterhook_windows(self):
        path, script, command = channels.make_script(
            COMMAND_0_HOOKS_AFTER, 'rOOt', 'windows'
        )
        self.assertIn('@type', script)
        self.assertTrue(path.startswith('rOOt'))

    def test_ms_firststep_beforehook_linux(self):
        path, script, command = channels.make_script(
            COMMAND_0_HOOKS_BEFORE, None, 'linux'
        )
        self.assertIn('touch', script)
        self.assertIn('OPENTF_WORKSPACE="`pwd`/."', script)
        self.assertTrue(path.startswith(channels.SCRIPTPATH_DEFAULT['linux']))

    def test_ms_firststep_beforehook_windows(self):
        path, script, command = channels.make_script(
            COMMAND_0_HOOKS_BEFORE, 'rOOt', 'windows'
        )
        self.assertIn('@type', script)
        self.assertIn('"OPENTF_WORKSPACE=%CD%\\."', script)
        self.assertTrue(path.startswith('rOOt'))

    def test_ms_teardown_keep_linux(self):
        path, script, command = channels.make_script(
            COMMAND_TEARDOWN_HOOKS_KEEP, None, 'linux'
        )
        self.assertNotIn('rm -rf', script)

    def test_ms_teardown_keep_windows(self):
        path, script, command = channels.make_script(
            COMMAND_TEARDOWN_HOOKS_KEEP, 'rOOt', 'windows'
        )
        self.assertNotIn('rmdir', script)

    def test_ms_teardown_donotkeep_linux(self):
        path, script, command = channels.make_script(
            COMMAND_TEARDOWN_HOOKS_DONOTKEEP, None, 'linux'
        )
        self.assertIn('rm -rf', script)

    def test_ms_teardown_donotkeep_windows(self):
        path, script, command = channels.make_script(
            COMMAND_TEARDOWN_HOOKS_DONOTKEEP, 'rOOt', 'windows'
        )
        self.assertIn('rmdir', script)

    def test_ms_firststep_nohook_linux(self):
        path, script, command = channels.make_script(COMMAND_0_NOHOOKS, None, 'linux')
        self.assertIn('touch', script)
        self.assertTrue(path.startswith(channels.SCRIPTPATH_DEFAULT['linux']))

    def test_ms_firststep_nohook_windows(self):
        path, script, command = channels.make_script(
            COMMAND_0_NOHOOKS, 'rOOt', 'windows'
        )
        self.assertIn('@type', script)
        self.assertTrue(path.startswith('rOOt'))

    def test_ms_firststep_nohook_workingdirectory_linux(self):
        path, script, command = channels.make_script(
            COMMAND_0_WD_NOHOOKS, 'rOOt', 'linux'
        )
        self.assertIn('touch', script)
        self.assertIn('cd wORking/DireCtory', script)

    def test_ms_firststep_nohook_workingdirectory_windows(self):
        path, script, command = channels.make_script(
            COMMAND_0_WD_NOHOOKS, None, 'windows'
        )
        self.assertIn('@type', script)
        self.assertIn('@cd wORking\\DireCtory', script)

    def test_ms_5(self):
        path, script, command = channels.make_script(COMMAND_1, 'rOoT', 'linux')
        self.assertNotIn('touch', script)

    def test_ms_5_local_vars(self):
        path, script, command = channels.make_script(
            COMMAND_1_LOCAL_VAR, 'rOoT', 'linux'
        )
        self.assertIn(
            'export vAr2=value\n. "$OPENTF_VARIABLES"\nexport vAr=vALuE', script
        )

    def test_ms_6(self):
        path, script, command = channels.make_script(COMMAND_1, 'rOoT', 'windows')
        self.assertNotIn('@type', script)

    def test_ms_6_local_vars(self):
        path, script, command = channels.make_script(
            COMMAND_1_LOCAL_VAR, 'rOoT', 'windows'
        )
        self.assertIn(
            '@set "vAr2=value"\r\ncall "%OPENTF_VARIABLES%"\r\n@set "vAr=vALuE"', script
        )

    def test_ms_7(self):
        path, script, command = channels.make_script(COMMAND_1_WD, 'rOoT', 'linux')
        self.assertNotIn('touch', script)
        self.assertIn('cd wORking/DireCtory', script)

    def test_ms_8(self):
        path, script, command = channels.make_script(COMMAND_1_WD, 'rOoT', 'windows')
        self.assertNotIn('@type', script)
        self.assertIn('@cd wORking\\DireCtory', script)

    def test_ms_7_spaces(self):
        path, script, command = channels.make_script(
            COMMAND_1_WD_SPACES, 'rOoT', 'linux'
        )
        self.assertNotIn('touch', script)
        self.assertIn('cd "wORk ing/DireCtory"', script)

    def test_ms_8_spaces(self):
        path, script, command = channels.make_script(
            COMMAND_1_WD_SPACES, 'rOoT', 'windows'
        )
        self.assertNotIn('@type', script)
        self.assertIn('@cd "wORk ing\\DireCtory"', script)

    def test_ms_9(self):
        path, script, command = channels.make_script(COMMAND_1_VAR, 'rOoT', 'linux')
        self.assertNotIn('touch', script)
        self.assertIn('export vAr=vALuE', script)

    def test_ms_10(self):
        path, script, command = channels.make_script(COMMAND_1_VAR, 'rOoT', 'windows')
        self.assertNotIn('@type', script)
        self.assertIn('@set "vAr=vALuE"', script)

    def test_ms_11(self):
        path, script, command = channels.make_script(COMMAND_1_VAR_WD, 'rOoT', 'linux')
        self.assertNotIn('touch', script)
        self.assertIn('export vAr=vALuE', script)
        self.assertIn('cd wORking/DireCtory', script)

    def test_ms_12(self):
        path, script, command = channels.make_script(
            COMMAND_1_VAR_WD, 'rOoT', 'windows'
        )
        self.assertNotIn('@type', script)
        self.assertIn('@cd wORking\\DireCtory', script)
        self.assertIn('@set "vAr=vALuE"', script)

    def test_ms_shell_wd(self):
        path, script, command = channels.make_script(
            COMMAND_SHELL_VAR_WD, 'rOOt', 'windows'
        )
        self.assertNotIn('@type', script)
        self.assertIn('@cd wORking\\DireCtory', script)
        self.assertIn('@set "vAr=vALuE"', script)
        self.assertIn('python', script)

    def test_ms_shell_wd_linux(self):
        path, script, command = channels.make_script(
            COMMAND_SHELL_VAR_WD, 'rOOt', 'linux'
        )
        self.assertNotIn('touch', script)
        self.assertIn('cd wORking/DireCtory', script)
        self.assertIn('export vAr=vALuE', script)
        self.assertIn('python', script)

    # mask

    def test_mask(self):
        jobstate = channels.JobState()
        jobstate.masks.append('BAR')
        what = channels.mask('fooBARbaz', jobstate)
        self.assertEqual(what, 'foo***baz')

    # process_output

    def test_process_output_1(self):
        mock_attach = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        _ = channels.process_output(
            COMMAND_0_NOHOOKS, 0, ['hello world'], [], jobstate, mock_attach, mock_put
        )
        self.assertFalse(jobstate.masks)
        mock_attach.assert_called_once()
        mock_put.assert_not_called()

    def test_process_output_debug(self):
        mock_attach = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        result = channels.process_output(
            COMMAND_0_NOHOOKS,
            0,
            ['::debug::foo', 'hello world'],
            ['ooops'],
            jobstate,
            mock_attach,
            mock_put,
        )
        self.assertIn('DEBUG,foo', result['logs'])
        self.assertFalse(result.get('outputs'))
        self.assertFalse(result['status'])
        mock_attach.assert_called_once()
        mock_put.assert_not_called()

    def test_process_output_attach(self):
        mock_attach = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        result = channels.process_output(
            COMMAND_0_NOHOOKS,
            0,
            ['::attach::foo/bar', 'hello world'],
            [],
            jobstate,
            mock_attach,
            mock_put,
        )
        self.assertFalse(result.get('outputs'))
        self.assertEqual(mock_attach.call_count, 2)
        self.assertEqual('/tmp/jOb_iD_dynamic_env.sh', mock_attach.call_args[0][0])
        first_args = mock_attach.call_args_list[0][0]
        self.assertEqual(first_args[0], 'foo/bar')
        first, _, second = first_args[1].partition('-')
        self.assertEqual('/tmp/jOb_iD', first)
        self.assertEqual('0_bar', second.partition('_')[2])
        self.assertTrue(re.match(UUID_REGEX, second.partition('_')[0]))
        mock_put.assert_not_called()

    def test_process_output_put(self):
        mock_attach = MagicMock()
        mock_put = MagicMock(return_value=2)
        mock_os = MagicMock()
        mock_os.path = MagicMock()
        mock_os.path.exists = MagicMock(return_value=True)
        jobstate = channels.JobState()
        with patch('opentf.toolkit.channels.os', mock_os):
            result = channels.process_output(
                COMMAND_0_NOHOOKS,
                0,
                ['::put file=yada::foo/bar', 'hello world'],
                [],
                jobstate,
                mock_attach,
                mock_put,
            )
        self.assertFalse(result.get('outputs'))
        mock_attach.assert_called_once()
        mock_put.assert_called_once_with('jOb_iD/foo/bar', '/tmp/in_wOrKflow_iD_yada')
        self.assertEqual(result['status'], 0)

    def test_process_output_put_badfile(self):
        mock_attach = MagicMock()
        mock_put = MagicMock(return_value=2)
        mock_os = MagicMock()
        mock_os.path = MagicMock()
        mock_os.path.exists = MagicMock(return_value=False)
        jobstate = channels.JobState()
        with patch('opentf.toolkit.channels.os', mock_os):
            result = channels.process_output(
                COMMAND_0_NOHOOKS,
                0,
                ['::put file=yada::foo/bar', 'hello world'],
                [],
                jobstate,
                mock_attach,
                mock_put,
            )
        self.assertFalse(result.get('outputs'))
        mock_attach.assert_called_once()
        mock_put.assert_not_called()
        self.assertEqual(result['status'], 2)

    def test_process_output_mask(self):
        mock_attach = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        result = channels.process_output(
            COMMAND_0_NOHOOKS,
            0,
            ['::add-mask::foo', 'hello foo world'],
            [],
            jobstate,
            mock_attach,
            mock_put,
        )
        self.assertFalse(result.get('outputs'))
        self.assertEqual(len(jobstate.masks), 1)
        self.assertNotIn('hello foo world', result['logs'])
        self.assertIn('hello *** world', result['logs'])

    def test_process_output_stopcommand(self):
        mock_attach = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        result = channels.process_output(
            COMMAND_0_NOHOOKS,
            0,
            [
                '::stop-commands::yada',
                '::add-mask::foo',
                '::yada::',
                '::attach type=something::/bar/baz',
                'hello foo world',
            ],
            [],
            jobstate,
            mock_attach,
            mock_put,
        )
        self.assertFalse(result.get('outputs'))
        self.assertEqual(len(jobstate.masks), 0)
        self.assertIn('hello foo world', result['logs'])
        self.assertNotIn('hello *** world', result['logs'])
        # mock_attach.assert_called_once_with('/bar/baz', '/tmp/jOb_iD_0_baz')
        self.assertEqual(mock_attach.call_count, 2)
        first_args = mock_attach.call_args_list[0][0]
        self.assertEqual('/bar/baz', first_args[0])
        first, _, second = first_args[1].partition('-')
        self.assertEqual('/tmp/jOb_iD', first)
        self.assertEqual('0_baz', second.partition('_')[2])
        self.assertTrue(re.match(UUID_REGEX, second.partition('_')[0]))
        self.assertIn('type', result['metadata']['attachments'][f'{first}-{second}'])

    def test_process_output_set_output(self):
        mock_attach = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        result = channels.process_output(
            COMMAND_0_NOHOOKS,
            0,
            [
                '::set-output  name=foo::yada',
                '::set-output name=bar::baz',
                'hello foo world\n\r   \t   \r',
            ],
            ['something on stderr\n  \t'],
            jobstate,
            mock_attach,
            mock_put,
        )
        self.assertEqual(len(jobstate.masks), 0)
        self.assertIn('hello foo world', result['logs'])
        self.assertIn('something on stderr', result['logs'])
        self.assertEqual(len(result['outputs']), 2)
        self.assertIn('foo', result['outputs'])
        self.assertEqual(result['outputs']['foo'], 'yada')
        self.assertIn('bar', result['outputs'])
        mock_attach.assert_called_once()
        mock_put.assert_not_called()

    def test_process_output_upload(self):
        mock_attach = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        result = channels.process_output(
            COMMAND_0_NOHOOKS,
            0,
            ['::upload::foo/bar', 'hello world'],
            [],
            jobstate,
            mock_attach,
            mock_put,
        )
        self.assertFalse(result.get('outputs'))
        self.assertEqual(0, result['metadata']['upload'])
        self.assertEqual(mock_attach.call_count, 2)
        first_args = mock_attach.call_args_list[0][0]
        self.assertEqual('foo/bar', first_args[0])
        first, _, second = first_args[1].partition('-')
        self.assertEqual('/tmp/wOrKflow_iD', first)
        self.assertEqual('WR_bar', second.partition('_')[2])
        self.assertTrue(re.match(UUID_REGEX, second.partition('_')[0]))
        mock_put.assert_not_called()

    def test_process_output_download_file(self):
        mock_get = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        result = channels.process_output(
            cmd,
            0,
            ['::download file=1.txt::/home/user/2.txt', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        self.assertIsNone(result['metadata'].get('artifacts'))
        mock_put.assert_called_once_with('/home/user/2.txt', '/tmp/123_WR_1.txt')

    def test_process_output_download_pattern(self):
        mock_get = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        channels.process_output(
            cmd,
            0,
            ['::download pattern=*.txt::/home/user', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        self.assertEqual(2, mock_put.call_count)
        self.assertEqual(
            ('/home/user/1.txt', '/tmp/123_WR_1.txt'), mock_put.call_args_list[0][0]
        )
        self.assertEqual(
            ('/home/user/2.txt', '/tmp/456_WR_2.txt'), mock_put.call_args_list[1][0]
        )

    def test_process_output_download_file_workspace(self):
        mock_get = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        channels.process_output(
            cmd,
            0,
            ['::download file=2.txt::', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        mock_put.assert_called_once_with('jOb_iD/2.txt', '/tmp/456_WR_2.txt')

    def test_process_output_download_defaults(self):
        mock_get = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        channels.process_output(
            cmd,
            0,
            ['::download::', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        self.assertEqual(2, mock_put.call_count)
        self.assertEqual(
            ('jOb_iD/1.txt', '/tmp/123_WR_1.txt'), mock_put.call_args_list[0][0]
        )
        self.assertEqual(
            ('jOb_iD/2.txt', '/tmp/456_WR_2.txt'), mock_put.call_args_list[1][0]
        )

    def test_process_output_download_no_artifacts(self):
        mock_get = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        del cmd['metadata']['artifacts']
        result = channels.process_output(
            cmd,
            0,
            ['::download pattern=*::', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        self.assertEqual(2, result['status'])
        self.assertIn('No artifact matching', result['logs'][0])

    def test_process_output_download_params_ko(self):
        mock_get = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        result = channels.process_output(
            cmd,
            0,
            ['::download a b c::', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        self.assertEqual(2, result['status'])
        self.assertIn('Invalid workflow command parameter', result['logs'][0])

    def test_process_output_download_exception(self):
        mock_get = MagicMock()
        mock_put = MagicMock(side_effect=Exception('Boom'))
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        result = channels.process_output(
            cmd,
            0,
            ['::download file=1.txt::', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        self.assertEqual(2, result['status'])
        self.assertIn('Boom', result['logs'][0])

    def test_process_output_no_matching_artifact(self):
        mock_get = MagicMock()
        mock_put = MagicMock()
        jobstate = channels.JobState()
        cmd = json.loads(json.dumps(COMMAND_1_ARTIFACTS))
        result = channels.process_output(
            cmd,
            0,
            ['::download file=dkjsjfksl::', 'hello world'],
            [],
            jobstate,
            mock_get,
            mock_put,
        )
        self.assertEqual(2, result['status'])
        self.assertIn(
            'No artifact matching requested name/pattern found', result['logs'][0]
        )

    # process_upload

    def test_process_upload_ok(self):
        result = RESULT_UPLOAD_ONLY.copy()
        with patch('opentf.toolkit.channels.make_event') as mock_event:
            channels.process_upload(result)
        mock_event.assert_called_once()
        wfr = mock_event.call_args
        self.assertNotIn('attachments', result)
        self.assertNotIn('upload', result)
        self.assertNotIn('attachments', result['metadata'])
        self.assertEqual(
            {'/tmp/wfid_att1': {'uuid': 'uuid1'}}, wfr[1]['metadata']['attachments']
        )
        self.assertEqual(['/tmp/wfid_att1'], wfr[1]['attachments'])

    def test_process_upload_ok_with_attach(self):
        result = RESULT_UPLOAD_ATTACH.copy()
        with patch('opentf.toolkit.channels.make_event') as mock_event:
            channels.process_upload(result)
        mock_event.assert_called_once()
        wfr = mock_event.call_args
        self.assertNotIn('upload', result)
        self.assertEqual(['/tmp/jobid_att2'], result['attachments'])
        self.assertEqual(
            {'/tmp/jobid_att2': {'uuid': 'uuid2'}}, result['metadata']['attachments']
        )
        self.assertEqual(
            {'/tmp/wfid_att1': {'uuid': 'uuid1'}}, wfr[1]['metadata']['attachments']
        )
        self.assertEqual(['/tmp/wfid_att1'], wfr[1]['attachments'])

    # process_opentf_variables

    def test_process_opentf_variables_ok(self):
        result = RESULT_VARS.copy()
        with patch(
            'builtins.open',
            new_callable=mock_open,
            read_data='VAR1=value1\nVAR2=value2',
        ):
            channels.process_opentf_variables(result)
        self.assertEqual({'VAR1': 'value1', 'VAR2': 'value2'}, result['variables'])
        self.assertIsNone(result['metadata'].get('attachments'))
        self.assertIsNone(result.get('attachments'))

    def test_process_opentf_variables_and_attachment_ok(self):
        result = RESULT_VARS_ATTACH.copy()
        with patch(
            'builtins.open',
            new_callable=mock_open,
            read_data='VAR3=value3\nVAR4=value4',
        ):
            channels.process_opentf_variables(result)
        self.assertEqual({'VAR3': 'value3', 'VAR4': 'value4'}, result['variables'])
        self.assertEqual(
            'uuid2', result['metadata']['attachments']['/tmp/jobid_att2']['uuid']
        )
        self.assertEqual(['/tmp/jobid_att2'], result['attachments'])

    # _get_opentf_variables

    def test_get_opentf_variables_set(self):
        data = 'set VAR5=value5\nset "VAR6=value6"'
        with patch('builtins.open', new_callable=mock_open, read_data=data), patch(
            'opentf.toolkit.channels.os.remove'
        ) as mock_remove:
            res = channels._get_opentf_variables('path')
        self.assertEqual({'VAR5': 'value5', 'VAR6': 'value6'}, res)
        mock_remove.assert_called_once()

    def test_get_opentf_variables_export(self):
        data = 'export VAR7=value7\nexport VAR8="value 8"'
        with patch('builtins.open', new_callable=mock_open, read_data=data):
            res = channels._get_opentf_variables('path')
        self.assertEqual({'VAR7': 'value7', 'VAR8': '"value 8"'}, res)

    def test_get_opentf_variables_multiline(self):
        data = 'VAR9=value9\nsome content\nsome another content\nVAR10=value10'
        with patch('builtins.open', new_callable=mock_open, read_data=data):
            res = channels._get_opentf_variables('path')
        self.assertEqual({'VAR10': 'value10', 'VAR9': 'value9'}, res)
