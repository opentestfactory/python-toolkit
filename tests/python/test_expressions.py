# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Expressions unit tests."""

import unittest

from opentf.commons import expressions


CONTEXTS_1 = {'variables': {'foo': 'fOO', 'bar': 'bAR'}}
CONTEXTS_1_VERBATIM = {
    'variables': {'foo': 'fOO', 'bar': {'value': 'bAR', 'verbatim': True}}
}

CONTEXTS_2 = {
    'variables': {'foo': 'fOO', 'bar': 'bAR'},
    'opentf': {
        'workflow': 'wORKFLOW',
        'job': 'jOB',
        'actor': 'me',
        'token': 'tOKEN',
        'step': 'sTEP',
    },
    'job': {'status': 'failure'},
    'steps': {
        'step_1': {
            'outputs': {'output_s1': 'oUTPUT_S1', 'output-s2': 'conclusion'},
            'outcome': 'failure',
            'conclusion': 'failure',
        },
        'step_2': {'outcome': 'success', 'conclusion': 'success'},
        'step_3': {'outputs': {'output_s1': None}},
        'step_4': {'outputs': {'output_s1': False, 'output-s2': True}},
    },
    'runner': {'os': 'Linux', 'temp': '/tmp'},
    'need': {'job_1': {'result': 'success', 'outputs': {'output_j1': 'oOUTPUT_J1'}}},
}

ITEMS = {
    'zero': 'not an expression',
    'one': '${{ variables.bar }}',
    'two': 'Hi there, ${{ opentf.actor }}.',
    'three': [0, 1, '2', 3, '${{ variables.foo}}', [11, 22, '${{ 33 }}'], {'a': 12}],
    'four': {
        'four1': '${{ job.status }}',
        'four2': "${{ steps.step_2.outcome == 'success'}}",
    },
}


class TestExpressions(unittest.TestCase):
    def test_mres_plain(self):
        response = expressions._maybe_remove_expression_syntax('foo')
        self.assertEqual(response, 'foo')

    def test_mres_noextraspaces(self):
        response = expressions._maybe_remove_expression_syntax('${{foo}}')
        self.assertEqual(response, 'foo')

    def test_mres_withextraspaces(self):
        response = expressions._maybe_remove_expression_syntax('  ${{ foo  }}')
        self.assertEqual(response, 'foo')

    def test_ee_knownvariable(self):
        response = expressions.evaluate('variables.foo', CONTEXTS_1)
        self.assertEqual(response, 'fOO')

    def test_ee_unknown_variable(self):
        response = expressions.evaluate('variables.baz', CONTEXTS_1)
        self.assertEqual(response, '')

    def test_ee_index(self):
        response = expressions.evaluate("variables['foo']", CONTEXTS_1)
        self.assertEqual(response, 'fOO')

    def test_ee_invalidindex(self):
        self.assertRaises(ValueError, expressions.evaluate, 'variables[==]', CONTEXTS_1)

    def test_ee_nesteddereference(self):
        response = expressions.evaluate('steps.step_1.outcome', CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_indexeddereference(self):
        response = expressions.evaluate("steps['step_1'].outcome", CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_dereferenceindexed(self):
        response = expressions.evaluate("steps.step_1['outcome']", CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_nestedindex(self):
        response = expressions.evaluate("steps['step_1']['outcome']", CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_8(self):
        response = expressions.evaluate("'hi there'", CONTEXTS_2)
        self.assertEqual(response, 'hi there')

    def test_ee_9(self):
        response = expressions.evaluate("'hi ''foo'' yada'", CONTEXTS_2)
        self.assertEqual(response, "hi 'foo' yada")

    def test_ee_10(self):
        response = expressions.evaluate("8.23", CONTEXTS_2)
        self.assertEqual(response, 8.23)

    def test_ee_10_p(self):
        response = expressions.evaluate("(8.23)", CONTEXTS_2)
        self.assertEqual(response, 8.23)

    def test_ee_10_pp(self):
        response = expressions.evaluate("((8.23))", CONTEXTS_2)
        self.assertEqual(response, 8.23)

    def test_ee_11(self):
        response = expressions.evaluate("8.23 == 8.23", CONTEXTS_2)
        self.assertTrue(response)

    def test_ee_12(self):
        response = expressions.evaluate("8.23 != 8.23", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_13(self):
        response = expressions.evaluate("variables['foo'] == 'fOO'", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_14(self):
        response = expressions.evaluate("'fOO' == variables['foo']", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_14_p(self):
        response = expressions.evaluate("('fOO' == variables['foo'])", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_14_ci(self):
        response = expressions.evaluate("'foo' == variables['foo']", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_14_ci_p(self):
        response = expressions.evaluate("('foo') == variables['foo']", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_14_ci_p_p(self):
        response = expressions.evaluate("('foo') == (variables['foo'])", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_15(self):
        response = expressions.evaluate("'fOO' != variables['foo']", CONTEXTS_1)
        self.assertFalse(response)

    def test_ee_16(self):
        response = expressions.evaluate("'fON' <= variables['foo']", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_17(self):
        response = expressions.evaluate("'fON' >= variables['foo']", CONTEXTS_1)
        self.assertFalse(response)

    def test_ee_18(self):
        response = expressions.evaluate(
            "steps.step_1.outcome == steps.step_1.conclusion", CONTEXTS_2
        )
        self.assertTrue(response)

    def test_ee_19(self):
        response = expressions.evaluate(
            "steps.step_2[steps.step_1.outputs.output-s2] == steps.step_2.conclusion",
            CONTEXTS_2,
        )
        self.assertTrue(response)

    def test_ee_19_alternative(self):
        response = expressions.evaluate(
            "steps.step_2[steps.step_1.outputs['output-s2']] == steps.step_2.conclusion",
            CONTEXTS_2,
        )
        self.assertTrue(response)

    def test_ee_19_spaceinidentifiernotallowed(self):
        self.assertRaises(
            ValueError,
            expressions.evaluate,
            "steps.step_2[steps.step_1.outputs.output - s2] == steps.step_2.conclusion",
            CONTEXTS_2,
        )

    def test_ee_20_eq(self):
        response = expressions.evaluate("0x10 == 16", CONTEXTS_2)
        self.assertTrue(response)

    def test_ee_20_gt(self):
        response = expressions.evaluate("0x10 > 16", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_20_lt(self):
        response = expressions.evaluate("0x10 < 16", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_21(self):
        response = expressions.evaluate("'' == 0", CONTEXTS_2)
        self.assertTrue(response)

    def test_ee_22(self):
        response = expressions.evaluate("'0' == 0", CONTEXTS_2)
        self.assertTrue(response)

    def test_ee_22_no(self):
        response = expressions.evaluate("'1' == 0", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_23(self):
        response = expressions.evaluate("'17' == 0x11", CONTEXTS_2)
        self.assertTrue(response)

    def test_ee_24(self):
        response = expressions.evaluate(
            "steps.step_3.outputs.output_s1 == 0", CONTEXTS_2
        )
        self.assertTrue(response)

    def test_ee_25(self):
        response = expressions.evaluate(
            "steps.step_3.outputs.output_s1 != 0", CONTEXTS_2
        )
        self.assertFalse(response)

    def test_ee_26_false(self):
        response = expressions.evaluate(
            "steps.step_4.outputs.output_s1 == 0", CONTEXTS_2
        )
        self.assertTrue(response)

    def test_ee_27_true(self):
        response = expressions.evaluate(
            "steps.step_4.outputs.output-s2 == 0", CONTEXTS_2
        )
        self.assertFalse(response)

    def test_ee_28_true(self):
        response = expressions.evaluate("'abc' == 0", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_29_true(self):
        response = expressions.evaluate("steps.step_1 == 0", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_re_whole_true(self):
        response = expressions.evaluate("'foo' ~= '^foo$'", CONTEXTS_2)
        self.assertTrue(response)

    def test_ee_re_whole_false(self):
        response = expressions.evaluate("'foo' ~= '^fo$'", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_re_partial_true(self):
        response = expressions.evaluate("'foo' ~= 'oo'", CONTEXTS_2)
        self.assertTrue(response)

    def test_ee_re_partial_false(self):
        response = expressions.evaluate("'foo' ~= 'of'", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_re_mismatch(self):
        self.assertRaises(ValueError, expressions.evaluate, "123 ~= 'of'", CONTEXTS_2)

    def test_ee_re_mismatch2(self):
        self.assertRaises(ValueError, expressions.evaluate, "'foo' ~= 123", CONTEXTS_2)

    def test_ei_30(self):
        response = expressions.evaluate_items(ITEMS, CONTEXTS_2)
        self.assertEqual(set(response), set(ITEMS))
        self.assertEqual(response['one'], 'bAR')
        self.assertEqual(response['two'], 'Hi there, me.')
        self.assertNotEqual(response['three'], ITEMS['three'])
        self.assertEqual(response['four']['four1'], 'failure')
        self.assertTrue(response['four']['four2'])
        self.assertEqual(response['three'][4], 'fOO')
        self.assertEqual(response['three'][5][2], 33)

    def test_evaluate_bool_stringsmaybetrue(self):
        self.assertTrue(expressions.evaluate_bool('true && ${{ false }}', {}))

    def test_evaluate_bool_stringsmaybefalse(self):
        self.assertFalse(expressions.evaluate_bool('0${{ 0 }}', {}))

    def test_evaluate_bool_stringsmaybefalse_strings(self):
        self.assertFalse(expressions.evaluate_bool("0${{ '0' }}", {}))

    def test_eb_40(self):
        response = expressions.evaluate_bool(
            'always()', CONTEXTS_2, special_functions={'always': True}
        )
        self.assertTrue(response)

    def test_eb_40_bis(self):
        self.assertRaises(ValueError, expressions.evaluate_bool, 'always()', CONTEXTS_2)

    def test_eb_41(self):
        response = expressions.evaluate_bool(
            'success()', CONTEXTS_2, special_functions={'success': False}
        )
        self.assertFalse(response)

    def test_eb_42(self):
        response = expressions.evaluate_bool(
            'cancelled()', CONTEXTS_2, special_functions={'cancelled': False}
        )
        self.assertFalse(response)

    def test_eb_43(self):
        response = expressions.evaluate_bool(
            'failure()', CONTEXTS_2, special_functions={'failure': True}
        )
        self.assertTrue(response)

    def test_eb_44(self):
        response = expressions.evaluate_bool(
            'failure ( ) ', CONTEXTS_2, special_functions={'failure': True}
        )
        self.assertTrue(response)

    def test_eb_45(self):
        self.assertRaises(ValueError, expressions.evaluate_bool, 'failure(', CONTEXTS_2)

    def test_eb_46(self):
        self.assertRaises(ValueError, expressions.evaluate_bool, 'foo()', CONTEXTS_2)

    def test_eb_47(self):
        response = expressions.evaluate_bool("job.status == 'failure'", CONTEXTS_2)
        self.assertTrue(response)

    def test_eb_48(self):
        response = expressions.evaluate_bool("fAlSe", CONTEXTS_2)
        self.assertFalse(response)

    def test_eb_49(self):
        contexts = CONTEXTS_2.copy()
        contexts[expressions.SPECIAL_FUNCTIONS] = {'always': 'aaa'}
        response = expressions.evaluate_bool('always()', contexts)
        self.assertTrue(response)
        self.assertIn('__called__', contexts[expressions.SPECIAL_FUNCTIONS])

    def test_eb_49bis(self):
        contexts = CONTEXTS_2.copy()
        contexts[expressions.SPECIAL_FUNCTIONS] = {'always': 'aaa'}
        response = expressions.evaluate_bool('1 == 1', contexts)
        self.assertTrue(response)
        self.assertNotIn('__called__', contexts[expressions.SPECIAL_FUNCTIONS])

    def test_eb_49ter(self):
        contexts = CONTEXTS_2.copy()
        response = expressions.evaluate_bool('success()', contexts, {'success': True})
        self.assertTrue(response)

    def test_eb_49ter_ve(self):
        contexts = CONTEXTS_2.copy()
        contexts[expressions.SPECIAL_FUNCTIONS] = {'success': False}
        self.assertRaisesRegex(
            ValueError,
            'Unknown function failure()',
            expressions.evaluate_bool,
            'failure()',
            contexts,
        )

    def test_ee_50(self):
        self.assertRaises(ValueError, expressions.evaluate, '1+2', CONTEXTS_1)

    def test_ee_51(self):
        self.assertRaises(ValueError, expressions.evaluate, '1 2', CONTEXTS_1)

    def test_ee_52(self):
        self.assertRaises(ValueError, expressions.evaluate, "'1' '2'", CONTEXTS_1)

    def test_ee_53(self):
        self.assertRaises(
            ValueError, expressions.evaluate, "job.status '2'", CONTEXTS_2
        )

    def test_ee_54(self):
        self.assertRaises(ValueError, expressions.evaluate, "==", CONTEXTS_2)

    def test_ee_55(self):
        self.assertRaises(
            ValueError, expressions.evaluate, "job.status.'abc'", CONTEXTS_2
        )

    def test_ee_and(self):
        response = expressions.evaluate("(8.23 != 8.23) && true", CONTEXTS_1)
        self.assertFalse(response)

    def test_ee_and_2(self):
        self.assertRaises(
            ValueError, expressions.evaluate, "(8.23 != 8.23 && true", CONTEXTS_1
        )

    def test_ee_and_3(self):
        self.assertRaises(
            ValueError, expressions.evaluate, "((8.23 != 8.23) && true", CONTEXTS_1
        )

    def test_ee_and_4(self):
        self.assertRaises(
            ValueError, expressions.evaluate, "(8.23 != 8.23) && (true", CONTEXTS_1
        )

    def test_ee_and_5(self):
        response = expressions.evaluate(
            "(steps.step_1.outcome == steps.step_1.conclusion) && (steps.step_1.outcome != steps.step_1.conclusion)",
            CONTEXTS_2,
        )
        self.assertFalse(response)

    def test_ee_and_6(self):
        response = expressions.evaluate("true && true && false", CONTEXTS_1)
        self.assertFalse(response)

    def test_ee_and_7(self):
        response = expressions.evaluate("true && true && false && true", CONTEXTS_1)
        self.assertFalse(response)

    def test_ee_and_8(self):
        response = expressions.evaluate("true && true && true", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_or(self):
        response = expressions.evaluate("(8.23 != 8.23) || true", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_or_2(self):
        response = expressions.evaluate(
            "(steps.step_1.outcome == steps.step_1.conclusion) || (steps.step_1.outcome != steps.step_1.conclusion)",
            CONTEXTS_2,
        )
        self.assertTrue(response)

    def test_ee_or_2_bis(self):
        response = expressions.evaluate(
            "(steps.step_1.outcome != steps.step_1.conclusion) || (steps.step_1.outcome == steps.step_1.conclusion)",
            CONTEXTS_2,
        )
        self.assertTrue(response)

    def test_ee_and_or(self):
        response = expressions.evaluate("(true && false) || (true && true)", CONTEXTS_1)
        self.assertTrue(response)

    def test_ee_and_or_2(self):
        response = expressions.evaluate(
            "(true && false) || (false && true)", CONTEXTS_1
        )
        self.assertFalse(response)

    def test_ee_and_or_3(self):
        response = expressions.evaluate(
            "true || (true && false) || (false && true)", CONTEXTS_1
        )
        self.assertTrue(response)

    def test_eeo_1(self):
        self.assertRaises(ValueError, expressions.evaluate_operation, 'foo', '*', 'bar')

    def test_ee_missing_rhs(self):
        self.assertRaises(ValueError, expressions.evaluate, 'true &&', {})

    def test_ee_missing_rparen(self):
        self.assertRaises(ValueError, expressions.evaluate, '(true && true', {})

    def test_ee_missing_rparen_2(self):
        self.assertRaises(ValueError, expressions.evaluate, 'true && (true', {})

    def test_ee_missing_lparen(self):
        self.assertRaises(ValueError, expressions.evaluate, 'true && true)', {})

    def test_ee_null(self):
        response = expressions.evaluate('null ||true', {})
        self.assertTrue(response)

    def test_ee_missingsecond(self):
        self.assertRaisesRegex(
            ValueError,
            r'Invalid token, was expecting expression',
            expressions.evaluate,
            'foobar(1,)',
            {},
        )

    def test_ee_third(self):
        self.assertRaisesRegex(
            ValueError,
            r'Invalid token, was expecting operator or "\)"',
            expressions.evaluate,
            'foobar(1,2,)',
            {},
        )

    def test_ee_function_two_parameters_unended(self):
        self.assertRaisesRegex(
            ValueError,
            r'Unexpected end of expression, was expecting operator or "\)"',
            expressions.evaluate,
            'foobar(1,2',
            {},
        )

    def test_ee_function_too_many_parameters(self):
        self.assertRaisesRegex(
            ValueError,
            r'Invalid token, was expecting operator or "\)"',
            expressions.evaluate,
            'foobar(1,2,3)',
            {},
        )

    def test_ee_function_arity_1_unknown(self):
        self.assertRaisesRegex(
            ValueError,
            r'Unknown function foobar\(arg\)',
            expressions.evaluate,
            'foobar(1)',
            {},
        )

    def test_ee_function_arity_2_unknown(self):
        self.assertRaisesRegex(
            ValueError,
            r'Unknown function foobar\(arg1, arg2\)',
            expressions.evaluate,
            "foobar(1,'2,3')",
            {},
        )

    def test_ee_function_contains_string(self):
        response = expressions.evaluate("contains('1,2',',2')", {})
        self.assertTrue(response)

    def test_ee_function_contains_string_ci(self):
        response = expressions.evaluate("contains('aBc','bc')", {})
        self.assertTrue(response)

    def test_ee_function_contains_string_ci_nomatch(self):
        response = expressions.evaluate("contains('aBc','cd')", {})
        self.assertFalse(response)

    def test_ee_function_contains_object_ci(self):
        response = expressions.evaluate(
            "contains(foo,'bc')", {'foo': {'AB': 12, 'BC': 34}}
        )
        self.assertTrue(response)

    def test_ee_function_contains_object_convert_int(self):
        response = expressions.evaluate(
            "contains(foo,12)", {'foo': {'12': 12, '34': 34}}
        )
        self.assertTrue(response)

    def test_ee_function_contains_object_convert_bool(self):
        response = expressions.evaluate(
            "contains(foo,1 == 1)", {'foo': {'12': 12, '34': 34, 'True': 'oh no'}}
        )
        self.assertTrue(response)

    def test_ee_not_true(self):
        self.assertFalse(expressions.evaluate('!true', {}))

    def test_ee_not_false(self):
        self.assertTrue(expressions.evaluate('!false', {}))

    def test_ee_not_exist(self):
        self.assertFalse(expressions.evaluate('!foo.bar', {'foo': {'bar': 'hi there'}}))

    def test_ee_not_empty_is_true(self):
        self.assertTrue(expressions.evaluate('!foo.baz', {'foo': {}}))

    def test_ee_ternary_true(self):
        self.assertEqual(
            expressions.evaluate(
                "foo.bar == 'baz' && 'yay' || 'nay'", {'foo': {'bar': 'baz'}}
            ),
            'yay',
        )

    def test_ee_ternary_false(self):
        self.assertEqual(
            expressions.evaluate(
                "foo.bar != 'baz' && 'yay' || 'nay'", {'foo': {'bar': 'baz'}}
            ),
            'nay',
        )

    def test_ee_function_contains_mixed(self):
        response = expressions.evaluate(
            """contains(fromJSON('["ABC", "def"]'), 'abc')""", {}
        )
        self.assertTrue(response)

    def test_ee_function_contains_mixed_nostrings(self):
        response = expressions.evaluate(
            """contains(fromJSON('["ABC", "def"]'), 'abc"')""", {}
        )
        self.assertFalse(response)

    def test_ee_function_contains_mixed_nope(self):
        response = expressions.evaluate(
            """contains(fromJSON('["abc", "def"]'), 123)""", {}
        )
        self.assertFalse(response)

    def test_ee_function_tojson(self):
        response = expressions.evaluate("toJSON(foo)", {'foo': [1, 2, 3]})
        self.assertEqual(response, '[1, 2, 3]')

    def test_ee_function_startswith(self):
        response = expressions.evaluate("startsWith('foobar', 'FOO')", {})
        self.assertTrue(response)

    def test_ee_function_endswith(self):
        response = expressions.evaluate("endsWith('foobar', 'bAR')", {})
        self.assertTrue(response)

    def test_ee_function_startswith_nope(self):
        response = expressions.evaluate("startsWith('foobar', 'OO')", {})
        self.assertFalse(response)

    def test_ee_function_endswith_nope(self):
        response = expressions.evaluate("endsWith('foobar', 'bA')", {})
        self.assertFalse(response)

    def test_ee_function_hour(self):
        response = expressions.evaluate("hour('2019-01-01T12:34:56.789')", {})
        self.assertEqual(response, 12)

    def test_ee_function_week(self):
        response = expressions.evaluate("week('2019-01-01T12:34:56.789')", {})
        self.assertEqual(response, 1)

    def test_ee_function_day(self):
        response = expressions.evaluate("day('2019-01-01T12:34:56.789')", {})
        self.assertEqual(response, 1)

    def test_ee_function_dayofweek(self):
        response = expressions.evaluate("dayOfWeek('2019-01-01T12:34:56.789')", {})
        self.assertEqual(response, 'Tuesday')

    def test_ee_function_dayofweek_xx(self):
        response = expressions.evaluate("dayOfWeek('2023-06-30T12:34:56.789')", {})
        self.assertEqual(response, 'Friday')

    def test_ee_function_dayofweek_bad(self):
        response = expressions.evaluate("dayOfWeek('202x-06-30T12:34:56.789')", {})
        self.assertEqual(response, '')

    def test_ee_function_dayofweekiso(self):
        response = expressions.evaluate("dayOfWeekISO('2019-01-01T12:34:56.789')", {})
        self.assertEqual(response, 2)

    def test_ee_function_dayofweekiso_bad(self):
        response = expressions.evaluate("dayOfWeekISO('2023-06-30TT12:34:56.789')", {})
        self.assertEqual(response, '')

    def test_ee_function_month_bad(self):
        response = expressions.evaluate("month('2023-06-30TT12:34:56.789')", {})
        self.assertEqual(response, '')

    def test_ee_function_month_xx(self):
        response = expressions.evaluate("month('2023-06-30T12:34:56.789')", {})
        self.assertEqual(response, 6)

    def test_ee_function_year_bad(self):
        response = expressions.evaluate("year('2023-06-30TT12:34:56.789')", {})
        self.assertEqual(response, '')

    def test_ee_function_year_xx(self):
        response = expressions.evaluate("year('2023-06-30T12:34:56.789')", {})
        self.assertEqual(response, 2023)

    def test_ee_function_minute_bad(self):
        response = expressions.evaluate("minute('2023-06-30TT12:34:56.789')", {})
        self.assertEqual(response, '')

    def test_ee_function_minute_xx(self):
        response = expressions.evaluate("minute('2023-06-30T12:34:56.789')", {})
        self.assertEqual(response, 34)

    def test_ee_function_second_bad(self):
        response = expressions.evaluate("second('2023-06-30TT12:34:56.789')", {})
        self.assertEqual(response, '')

    def test_ee_function_second_xx(self):
        response = expressions.evaluate("second('2023-06-30T12:34:56.789')", {})
        self.assertEqual(response, 56)

    def test_ee_string_dereference(self):
        self.assertRaisesRegex(
            ValueError,
            'bar is not an object.  It does not have a baz entry',
            expressions.evaluate,
            "foo.bar.baz == 'nope'",
            {'foo': {'bar': 'oh no'}},
        )


if __name__ == '__main__':
    unittest.main()
