# Copyright (c) 2022-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Toolkit unit tests."""

import unittest

from collections import defaultdict
from unittest.mock import MagicMock, patch, mock_open

from opentf import toolkit


########################################################################

PROVIDER_COMMAND = {
    'apiVersion': 'opentestfactory.org/v1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'nAmE',
        'workflow_id': 'wOrKflow_iD',
        'job_id': 'jOb_iD',
        'job_origin': [],
        'step_id': 'sTEp_iD',
        'step_origin': [],
    },
    'contexts': {},
    'step': {
        'uses': 'foo/bar@v1',
    },
    'runs-on': ['windows'],
}

GENERATOR_COMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'GeneratorCommand',
    'generator': 'core/dummygenerator@v1',
    'metadata': {
        'name': 'nAmE',
        'workflow_id': 'wOrKflow_iD',
        'job_id': 'jOb_iD',
        'job_origin': [],
        'labels': {
            'opentestfactory.org/category': 'dummygenerator',
            'opentestfactory.org/categoryPrefix': 'core',
            'opentestfactory.org/categoryVersion': 'v1',
        },
    },
    'with': {'my_job_1': {'runs-on': 'linux', 'steps': [{'run': 'echo hi'}]}},
}

EXECUTION_COMMAND_REQUEST = {
    'apiVersion': 'opentestfactory.org/v1',
    'kind': 'ExecutionCommand',
    'metadata': {
        'name': 'nAmE',
        'workflow_id': 'wOrKflow_iD',
        'job_id': 'jOb_iD',
        'job_origin': [],
        'step_sequence_id': -1,
    },
    'runs-on': ['linux'],
    'scripts': [],
}

EXECUTION_COMMAND_BROKEN = {
    'apiVersion': 'opentestfactory.org/v1',
    'kind': 'ExecutionCommand',
}

EXECUTION_COMMAND_REGULAR = {
    'apiVersion': 'opentestfactory.org/v1',
    'kind': 'ExecutionCommand',
    'metadata': {
        'name': 'nAmE',
        'workflow_id': 'wOrKflow_iD',
        'job_id': 'jOb_iD',
        'job_origin': [],
        'step_sequence_id': 0,
        'channel_id': 'channel_Id',
    },
    'runs-on': ['linux'],
    'scripts': [],
}

PCV_METADATA_LABELS = {
    'labels': {
        'opentestfactory.org/categoryPrefix': 'p',
        'opentestfactory.org/category': 'c',
        'opentestfactory.org/categoryVersion': 'v',
    }
}


########################################################################


class TestToolkit(unittest.TestCase):
    # _get_target

    def test_make_target_category_exact(self):
        providers = {
            'function': 'a_function',
            'function@v1.1': 'a_function_11',
            'function@v1': 'a_function_1',
        }
        labels = {
            'opentestfactory.org/category': 'function',
            'opentestfactory.org/categoryPrefix': 'provider',
            'opentestfactory.org/categoryVersion': 'v1.1',
        }
        self.assertEqual(toolkit._get_target(labels, providers), 'a_function_11')

    def test_make_target_category_closest_patch(self):
        providers = {
            'function': 'a_function',
            'function@v1.1': 'a_function_11',
            'function@v1': 'a_function_1',
        }
        labels = {
            'opentestfactory.org/category': 'function',
            'opentestfactory.org/categoryPrefix': 'provider',
            'opentestfactory.org/categoryVersion': 'v1.1.2',
        }
        self.assertEqual(toolkit._get_target(labels, providers), 'a_function_11')

    def test_make_target_category_closest_minor(self):
        providers = {
            'function': 'a_function',
            'function@v1.1': 'a_function_11',
            'function@v1': 'a_function_1',
        }
        labels = {
            'opentestfactory.org/category': 'function',
            'opentestfactory.org/categoryPrefix': 'provider',
            'opentestfactory.org/categoryVersion': 'v1.2',
        }
        self.assertEqual(toolkit._get_target(labels, providers), 'a_function_1')

    def test_make_target_category_closest_major(self):
        providers = {
            'function': 'a_function',
            'function@v1.1': 'a_function_11',
            'function@v1': 'a_function_1',
        }
        labels = {
            'opentestfactory.org/category': 'function',
            'opentestfactory.org/categoryPrefix': 'provider',
            'opentestfactory.org/categoryVersion': 'v2',
        }
        self.assertEqual(toolkit._get_target(labels, providers), 'a_function')

    # make_plugin

    def test_make_plugin_1(self):
        mock_response = MagicMock()
        mock = MagicMock(return_value=mock_response)
        with patch('opentf.toolkit.make_app', mock), patch('sys.argv', ['dummy']):
            self.assertTrue(
                toolkit.make_plugin(
                    'nAmE', 'dEscripTIOn', provider=lambda: None, descriptor={}
                )
            )

    def test_make_plugin_2(self):
        mock_response = MagicMock()
        mock = MagicMock(return_value=mock_response)
        with patch('opentf.commons.make_app', mock), patch('sys.argv', ['dummy']):
            self.assertRaises(
                ValueError,
                toolkit.make_plugin,
                'nAmE',
                'dEscripTIOn',
                provider=lambda: None,
                descriptor='foo',
            )

    def test_make_plugin_3(self):
        mock_response = MagicMock()
        mock = MagicMock(return_value=mock_response)
        with patch('opentf.toolkit.make_app', mock), patch('sys.argv', ['dummy']):
            self.assertRaises(
                ValueError,
                toolkit.make_plugin,
                'nAmE',
                'dEscripTIOn',
                provider=lambda: None,
                generator=lambda: None,
                descriptor='foo',
            )

    def test_make_plugin_4(self):
        mock_publish = MagicMock()
        with patch('sys.argv', ['dummy', '--trusted-authorities', 'foo']), patch(
            'opentf.toolkit.core.publish_providerresult', mock_publish
        ):
            with patch('builtins.open', mock_open(read_data='foo')):
                plugin = toolkit.make_plugin(
                    'nAmE', 'dEscripTIOn', provider=lambda _: [], descriptor={}
                )
            plugin.config['CONTEXT']['enable_insecure_login'] = True
            plugin.config['CONTEXT'][toolkit.INPUTS_KEY] = {}
            plugin.config['CONTEXT'][toolkit.OUTPUTS_KEY] = {}
            plugin = plugin.test_client()
            plugin.post('/inbox', json=PROVIDER_COMMAND)
        mock_publish.assert_called_once()

    def test_make_plugin_delayed(self):
        mock_response = MagicMock()
        mock = MagicMock(return_value=mock_response)
        with patch('opentf.toolkit.make_app', mock), patch('sys.argv', ['dummy']):
            self.assertTrue(
                toolkit.make_plugin('nAmE', 'dEscripTIOn', providers={}, descriptor={})
            )

    def test_make_plugin_with_channel_noargs(self):
        mock_response = MagicMock()
        mock = MagicMock(return_value=mock_response)
        mock_env = MagicMock(return_value={'NAME_CHANNEL_HOOKS': '/foo/bar.yaml'})

        with patch('opentf.toolkit.make_app', mock), patch(
            'sys.argv', ['dummy']
        ), patch('os.environ', mock_env), patch(
            'opentf.toolkit._maybe_add_hook_watcher'
        ) as mock_mahw, patch(
            'opentf.toolkit.watch_file'
        ) as mock_wf:
            self.assertRaises(
                ValueError,
                toolkit.make_plugin,
                'nAmE',
                'dEscripTIOn',
                channel=lambda: None,
                descriptor={},
            )
            mock_mahw.assert_not_called()
            mock_wf.assert_called_once

    def test_make_plugin_with_channel_hooks(self):
        mock_response = MagicMock()
        mock = MagicMock(return_value=mock_response)
        mock_env = MagicMock(return_value={'NAME_CHANNEL_HOOKS': '/foo/bar.yaml'})
        mock_wf = MagicMock()

        with patch('opentf.toolkit.make_app', mock), patch(
            'sys.argv', ['dummy']
        ), patch('os.environ', mock_env), patch(
            'opentf.toolkit._maybe_add_hook_watcher'
        ) as mock_mahw, patch(
            'opentf.toolkit.watch_file', mock_wf
        ):
            self.assertTrue(
                toolkit.make_plugin(
                    'nAmE',
                    'dEscripTIOn',
                    channel=lambda: None,
                    descriptor={},
                    args=[{}],
                )
            )
            mock_mahw.assert_called_once_with(mock_response, toolkit.CHANNEL_HOOKS)
            mock_wf.assert_called_once

    def test_make_plugin_with_provider_hooks(self):
        mock_response = MagicMock()
        mock = MagicMock(return_value=mock_response)
        mock_env = MagicMock(return_value={'UNNAMED_PROVIDER_HOOKS': '/foo/bar.yaml'})
        mock_wf = MagicMock()

        with patch('opentf.toolkit.make_app', mock), patch(
            'sys.argv', ['dummy']
        ), patch('os.environ', mock_env), patch(
            'opentf.toolkit._maybe_add_hook_watcher'
        ) as mock_mahw, patch(
            'opentf.toolkit.watch_file', mock_wf
        ):
            self.assertTrue(
                toolkit.make_plugin(
                    'unNaMed', 'dEscripTIOn', provider=lambda: None, descriptor={}
                )
            )
            mock_mahw.assert_called_once_with(mock_response, toolkit.PROVIDERCONFIG)
            mock_wf.assert_called_once

    def test_make_plugin_generator(self):
        mock_publish = MagicMock()
        with patch('sys.argv', ['dummy', '--trusted-authorities', 'foo']), patch(
            'opentf.toolkit.core.publish_generatorresult', mock_publish
        ):
            with patch('builtins.open', mock_open(read_data='foo')):
                plugin = toolkit.make_plugin(
                    'nAmE', 'dEscripTIOn', generator=lambda _: [], descriptor={}
                )
            plugin.config['CONTEXT']['enable_insecure_login'] = True
            plugin.config['CONTEXT'][toolkit.INPUTS_KEY] = {}
            plugin.config['CONTEXT'][toolkit.OUTPUTS_KEY] = {}
            plugin = plugin.test_client()
            plugin.post('/inbox', json=GENERATOR_COMMAND)
        mock_publish.assert_called_once()

    def test_make_plugin_channel_request(self):
        mock_handler = MagicMock()
        mock_dispatch = MagicMock()
        with patch('sys.argv', ['dummy', '--trusted-authorities', 'foo']), patch(
            'opentf.toolkit._dispatch_executioncommand', mock_dispatch
        ):
            with patch('builtins.open', mock_open(read_data='foo')):
                plugin = toolkit.make_plugin(
                    'nAmE',
                    'dEscripTIOn',
                    channel=mock_handler,
                    descriptor={},
                    args=[{}],
                )
            plugin.config['CONTEXT']['enable_insecure_login'] = True
            plugin = plugin.test_client()
            plugin.post('/inbox', json=EXECUTION_COMMAND_REQUEST)
        mock_dispatch.assert_called_once()

    def test_make_plugin_channel_ontarget(self):
        mock_handler = MagicMock()
        mock_dispatch = MagicMock()
        with patch('sys.argv', ['dummy', '--trusted-authorities', 'foo']), patch(
            'opentf.toolkit._dispatch_executioncommand', mock_dispatch
        ):
            with patch('builtins.open', mock_open(read_data='foo')):
                plugin = toolkit.make_plugin(
                    'nAmE',
                    'dEscripTIOn',
                    channel=mock_handler,
                    descriptor={},
                    args=[{'channel_Id'}],
                )
            plugin.config['CONTEXT']['enable_insecure_login'] = True
            plugin = plugin.test_client()
            plugin.post('/inbox', json=EXECUTION_COMMAND_REGULAR)
        mock_dispatch.assert_called_once()

    def test_make_plugin_channel_notontarget(self):
        mock_handler = MagicMock()
        mock_dispatch = MagicMock()
        with patch('sys.argv', ['dummy', '--trusted-authorities', 'foo']), patch(
            'opentf.toolkit._dispatch_executioncommand', mock_dispatch
        ):
            with patch('builtins.open', mock_open(read_data='foo')):
                plugin = toolkit.make_plugin(
                    'nAmE',
                    'dEscripTIOn',
                    channel=mock_handler,
                    descriptor={},
                    args=[{'fOo'}],
                )
            plugin.config['CONTEXT']['enable_insecure_login'] = True
            plugin = plugin.test_client()
            plugin.post('/inbox', json=EXECUTION_COMMAND_REGULAR)
        mock_dispatch.assert_not_called()

    def test_make_plugin_channel_broken(self):
        mock_handler = MagicMock()
        mock_dispatch = MagicMock()
        with patch('sys.argv', ['dummy', '--trusted-authorities', 'foo']), patch(
            'opentf.toolkit._dispatch_executioncommand', mock_dispatch
        ), patch('opentf.toolkit.make_status_response'):
            with patch('builtins.open', mock_open(read_data='foo')):
                plugin = toolkit.make_plugin(
                    'nAmE',
                    'dEscripTIOn',
                    channel=mock_handler,
                    descriptor={},
                    args=[{'fOo'}],
                )
            plugin.config['CONTEXT']['enable_insecure_login'] = True
            plugin = plugin.test_client()
            plugin.post('/inbox', json=EXECUTION_COMMAND_BROKEN)
        mock_dispatch.assert_not_called()

    # _maybe_add_hook_watcher

    def test_maybe_add_hook_watcher_channel(self):
        template = {
            'events': [],
            'before': [{'run': '{name}_{type_}'}, {'run': '{name}_{type_}'}],
        }
        mock_plugin = MagicMock()
        mock_plugin.name = 'myplugin'
        mock_plugin.config = {'CONTEXT': {toolkit.KIND_KEY: toolkit.EXECUTIONCOMMAND}}
        env = {'MYPLUGIN_CHANNEL_HOOKS': '/foo/bar.yaml'}
        with patch('os.environ', env), patch(
            'opentf.toolkit.watch_file'
        ) as mock_watch_file, patch(
            'opentf.toolkit.INVALID_HOOKS_DEFINITION_TEMPLATE', template
        ):
            toolkit._maybe_add_hook_watcher(mock_plugin, 'sChemA')
        mock_watch_file.assert_called_once()
        self.assertEqual({'channel': 'setup'}, template['events'][0])
        self.assertEqual('MYPLUGIN_CHANNEL', template['before'][0]['run'])
        self.assertEqual('MYPLUGIN_CHANNEL', template['before'][1]['run'])

    def test_maybe_add_hook_watcher_provider(self):
        template = {
            'events': [],
            'before': [{'run': '{name}_{type_}'}, {'run': '{name}_{type_}'}],
        }
        mock_plugin = MagicMock()
        mock_plugin.name = 'myplugin'
        mock_plugin.config = {'CONTEXT': {toolkit.KIND_KEY: toolkit.PROVIDERCOMMAND}}
        env = {'MYPLUGIN_PROVIDER_HOOKS': '/foo/bar.yaml'}
        with patch('os.environ', env), patch(
            'opentf.toolkit.watch_file'
        ) as mock_watch_file, patch(
            'opentf.toolkit.INVALID_HOOKS_DEFINITION_TEMPLATE', template
        ):
            toolkit._maybe_add_hook_watcher(mock_plugin, 'sChemA')
        mock_watch_file.assert_called_once()
        self.assertEqual({'category': '_'}, template['events'][0])
        self.assertEqual('MYPLUGIN_PROVIDER', template['before'][0]['run'])
        self.assertEqual('MYPLUGIN_PROVIDER', template['before'][1]['run'])

    # _read_hooks_definition

    def test_rhd_nohooksfile(self):
        mock_plugin = MagicMock()
        mock_plugin.logger = MagicMock()
        mock_plugin.logger.error = MagicMock()
        toolkit._read_hooks_definition(
            mock_plugin, 'notafile', 'notaschema', {'ook': 'koo'}
        )
        mock_plugin.logger.error.assert_called_once()

    def test_rhd_nohooksinhooksfile(self):
        mock_logger = MagicMock()
        mock_logger.info = MagicMock()
        mock_logger.error = MagicMock()
        mock_plugin = MagicMock()
        mock_plugin.config = {'CONFIG': {'hooks': 'old'}}
        mock_plugin.logger = mock_logger
        mock_read = mock_open(read_data='- yada')
        with patch('builtins.open', mock_read):
            toolkit._read_hooks_definition(
                mock_plugin, 'afile', 'notaschema', {'ook': 'koo'}
            )
        mock_logger.error.assert_called_once_with(
            "Hooks definition file '%s' needs a 'hooks' entry, ignoring.", 'afile'
        )

    def test_rhd_badhooksfile_invalidhookdefinition(self):
        mock_logger = MagicMock()
        mock_logger.info = MagicMock()
        mock_logger.error = MagicMock()
        mock_plugin = MagicMock()
        mock_plugin.config = {'CONFIG': {'hooks': 'old'}}
        mock_plugin.logger = mock_logger
        mock_read = mock_open(read_data='hooks:\n- yada')
        with patch('builtins.open', mock_read), patch(
            'opentf.toolkit.validate_schema', MagicMock(return_value=(False, 'yada'))
        ):
            toolkit._read_hooks_definition(
                mock_plugin, 'afile', 'aschema', {'ook': 'koo'}
            )
        mock_logger.info.assert_called_once_with(
            "Replacing hooks definition using '%s'.", 'afile'
        )
        mock_logger.error.assert_called_once_with(
            "Error while verifying '%s' hooks definition: %s.", 'afile', 'yada'
        )
        self.assertEqual(mock_plugin.config['CONFIG']['hooks'], [{'ook': 'koo'}])

    def test_rhd_badhooksfile_noprevious(self):
        mock_logger = MagicMock()
        mock_logger.info = MagicMock()
        mock_logger.error = MagicMock()
        mock_plugin = MagicMock()
        mock_plugin.config = {'CONFIG': {}}
        mock_plugin.logger = mock_logger
        mock_read = mock_open(read_data='hooks:\n- yada')
        with patch('builtins.open', mock_read), patch(
            'opentf.toolkit.validate_schema', MagicMock(return_value=(False, 'yada'))
        ):
            toolkit._read_hooks_definition(
                mock_plugin, 'afile', 'aschema', {'ook': 'koo'}
            )
        mock_logger.info.assert_called_once_with(
            "Reading hooks definition from '%s'.", 'afile'
        )
        mock_logger.error.assert_called_once_with(
            "Error while verifying '%s' hooks definition: %s.", 'afile', 'yada'
        )
        self.assertEqual(mock_plugin.config['CONFIG']['hooks'], [{'ook': 'koo'}])

    # run_plugin

    def test_run_plugin_provider_category(self):
        mock_plugin = MagicMock()
        mock_plugin.name = 'myplugin'
        mock_plugin.config = {
            'CONTEXT': {
                '__kind key__': toolkit.PROVIDERCOMMAND,
                'eventbus': {'endpoint': 'eNDPoiNT', 'token': 'TOKeN'},
                'host': 'HosT',
                'port': 'PorT',
                'ssl_context': 'sSL_cONTexT',
            },
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'myplugin', 'action': 'aCTIOn'},
                    'events': [{'category': 'foo'}],
                }
            ],
        }
        mock_run = MagicMock()
        mock_subscribe = MagicMock(return_value='subscrIPTion')
        mock_unsubscribe = MagicMock()
        with patch('opentf.toolkit.run_app', mock_run), patch(
            'opentf.toolkit.subscribe', mock_subscribe
        ), patch('opentf.toolkit.unsubscribe', mock_unsubscribe):
            self.assertIsNone(toolkit.run_plugin(mock_plugin))
        mock_subscribe.assert_called_once_with(
            kind='opentestfactory.org/v1/ProviderCommand',
            target='inbox',
            app=mock_plugin,
            labels={
                'opentestfactory.org/category': 'foo',
            },
        )
        mock_unsubscribe.assert_called_once()
        mock_run.assert_called_once()

    def test_run_plugin_provider_category_categoryprefix(self):
        mock_plugin = MagicMock()
        mock_plugin.name = 'myplugin'
        mock_plugin.config = {
            'CONTEXT': {
                '__kind key__': toolkit.PROVIDERCOMMAND,
                'eventbus': {'endpoint': 'eNDPoiNT', 'token': 'TOKeN'},
                'host': 'HosT',
                'port': 'PorT',
                'ssl_context': 'sSL_cONTexT',
            },
            'DESCRIPTOR': [
                {'metadata': {'name': 'myplugin'}},
                {
                    'metadata': {'name': 'myplugin', 'action': 'aCTIOn'},
                    'events': [{'category': 'foo', 'categoryPrefix': 'bAr'}],
                },
            ],
        }
        mock_run = MagicMock()
        mock_subscribe = MagicMock(return_value='subscrIPTion')
        mock_unsubscribe = MagicMock()
        with patch('opentf.toolkit.run_app', mock_run), patch(
            'opentf.toolkit.subscribe', mock_subscribe
        ), patch('opentf.toolkit.unsubscribe', mock_unsubscribe):
            self.assertIsNone(toolkit.run_plugin(mock_plugin))
        mock_subscribe.assert_called_once_with(
            kind='opentestfactory.org/v1/ProviderCommand',
            target='inbox',
            app=mock_plugin,
            labels={
                'opentestfactory.org/category': 'foo',
                'opentestfactory.org/categoryPrefix': 'bAr',
            },
        )
        mock_unsubscribe.assert_called_once()
        mock_run.assert_called_once()

    def test_run_plugin_channel(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                '__kind key__': 'opentestfactory.org/v1/ExecutionCommand',
                '__manifest key__': [
                    {'metadata': {}},
                    {
                        'metadata': {'action': 'aCTIOn'},
                        'events': [{'category': 'foo', 'categoryPrefix': 'bAr'}],
                    },
                ],
                'eventbus': {'endpoint': 'eNDPoiNT', 'token': 'TOKeN'},
                'host': 'HosT',
                'port': 'PorT',
                'ssl_context': 'sSL_cONTexT',
            }
        }
        mock_run = MagicMock()
        mock_subscribe = MagicMock(return_value='subscrIPTion')
        mock_unsubscribe = MagicMock()
        with patch('opentf.toolkit.run_app', mock_run), patch(
            'opentf.toolkit.subscribe', mock_subscribe
        ), patch('opentf.toolkit.unsubscribe', mock_unsubscribe):
            self.assertIsNone(toolkit.run_plugin(mock_plugin))
        mock_subscribe.assert_called_once_with(
            kind='opentestfactory.org/v1/ExecutionCommand',
            target='inbox',
            app=mock_plugin,
        )
        mock_unsubscribe.assert_called_once()
        mock_run.assert_called_once()

    def test_run_plugin_generatorcommand(self):
        mock_plugin = MagicMock()
        mock_plugin.name = 'myplugin'
        mock_plugin.config = {
            'CONTEXT': {
                '__kind key__': toolkit.GENERATORCOMMAND,
                'eventbus': {'endpoint': 'eNDPoiNT', 'token': 'TOKeN'},
            },
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'myplugin', 'action': 'aCTIOn'},
                    'events': [
                        {
                            'category': 'foo',
                            'categoryPrefix': 'bar',
                            'categoryVersion': 'v22',
                        }
                    ],
                }
            ],
        }
        mock_run = MagicMock()
        mock_subscribe = MagicMock(return_value='subscrIPTion')
        mock_unsubscribe = MagicMock()
        with patch('opentf.toolkit.run_app', mock_run), patch(
            'opentf.toolkit.subscribe', mock_subscribe
        ), patch('opentf.toolkit.unsubscribe', mock_unsubscribe):
            self.assertIsNone(toolkit.run_plugin(mock_plugin))
        mock_subscribe.assert_called_once_with(
            kind='opentestfactory.org/v1alpha1/GeneratorCommand',
            target='inbox',
            labels={
                'opentestfactory.org/category': 'foo',
                'opentestfactory.org/categoryPrefix': 'bar',
                'opentestfactory.org/categoryVersion': 'v22',
            },
            app=mock_plugin,
        )
        mock_unsubscribe.assert_called_once()
        mock_run.assert_called_once()

    def test_run_plugin_generatorcommand_name_ko_action_ko_category_ko(self):
        mock_plugin = MagicMock()
        mock_plugin.name = 'myplugin'
        mock_plugin.config = {
            'CONTEXT': {
                '__kind key__': toolkit.GENERATORCOMMAND,
                'eventbus': {'endpoint': 'eNDPoiNT', 'token': 'TOKeN'},
            },
            'DESCRIPTOR': [
                {
                    'metadata': {'name': 'NotMyPlugin', 'action': 'aCTIOn'},
                },
                {
                    'metadata': {'name': 'myplugin'},
                },
                {
                    'metadata': {'name': 'myplugin', 'action': 'aCTIOn'},
                    'events': [{'categoryVersion': 'v22'}],
                },
            ],
        }
        mock_plugin.logger.warning = MagicMock()
        mock_run = MagicMock()
        mock_subscribe = MagicMock(return_value='subscrIPTion')
        mock_unsubscribe = MagicMock()
        mock_warning = MagicMock()
        with patch('opentf.toolkit.run_app', mock_run), patch(
            'opentf.toolkit.subscribe', mock_subscribe
        ), patch('opentf.toolkit.unsubscribe', mock_unsubscribe):
            self.assertIsNone(toolkit.run_plugin(mock_plugin))
        mock_subscribe.assert_not_called()
        mock_unsubscribe.assert_not_called()
        mock_run.assert_called_once()
        mock_plugin.logger.warning.assert_called_once_with(
            "At least one of 'category', 'categoryPrefix' required, ignoring."
        )

    # _dispatch_providercommand

    def test_dispatch_providercommand_1(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {toolkit.INPUTS_KEY: {}, toolkit.OUTPUTS_KEY: {}}
        }
        mock_publish_error = MagicMock()
        with patch('opentf.toolkit.core.publish_providerresult'), patch(
            'opentf.toolkit.core.publish_error', mock_publish_error
        ):
            toolkit._dispatch_providercommand(
                mock_plugin, lambda x: x, {'metadata': {}, 'step': {}}
            )
        mock_publish_error.assert_not_called()

    def test_dispatch_providercommand_2(self):
        mock_plugin = MagicMock()
        mock_publish_providerresult = MagicMock(
            side_effect=toolkit.core.ExecutionError('fOo')
        )
        mock_publish_error = MagicMock()
        with patch(
            'opentf.toolkit.core.publish_providerresult', mock_publish_providerresult
        ), patch('opentf.toolkit.core.publish_error', mock_publish_error):
            toolkit._dispatch_providercommand(
                mock_plugin, lambda x: x, {'metadata': {}, 'step': {}}
            )
        mock_publish_error.assert_called_once()

    def test_dispatch_providercommand_3(self):
        mock_plugin = MagicMock()
        mock_publish_providerresult = MagicMock(side_effect=Exception('fOo'))
        mock_publish_error = MagicMock()
        with patch(
            'opentf.toolkit.core.publish_providerresult', mock_publish_providerresult
        ), patch('opentf.toolkit.core.publish_error', mock_publish_error):
            toolkit._dispatch_providercommand(
                mock_plugin, lambda x: x, {'metadata': {}, 'step': {}}
            )
        mock_publish_error.assert_called_once()

    def test_dispatch_providercommand_with_ok_nonormalize(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {
                    ('p', 'c', 'v'): ({'abc-def': {}, 'ghi': {}}, False)
                },
                toolkit.OUTPUTS_KEY: {('p', 'c', 'v'): {'foo-bar': {}}},
            }
        }
        mock_handler = MagicMock()
        mock_handler.__name__ = 'mock_handler'
        body = {
            'metadata': PCV_METADATA_LABELS,
            'step': {'with': {'abc-def': 'foo', 'ghi': 'bar'}},
        }
        with patch('opentf.toolkit.core.publish_providerresult') as mock_ppr, patch(
            'opentf.toolkit.core.publish_error'
        ) as mock_publish_error:
            toolkit._dispatch_providercommand(mock_plugin, mock_handler, body)
        mock_publish_error.assert_not_called()
        mock_ppr.assert_called_once()
        args = mock_ppr.call_args[0]
        self.assertEqual(args[1], {'foo-bar': {}})
        mock_handler.assert_called_once_with(body['step']['with'])

    def test_dispatch_providercommand_with_ok_normalize(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {
                    ('p', 'c', 'v'): ({'abc-def': {}, 'ghi': {}}, False)
                },
                toolkit.OUTPUTS_KEY: {('p', 'c', 'v'): {'foo': 'fOo'}},
            }
        }
        mock_publish_error = MagicMock()
        mock_handler = MagicMock()
        mock_handler.__name__ = 'mock_handler'
        body = {
            'metadata': PCV_METADATA_LABELS,
            'step': {'with': {'abc_def': 'foo', 'ghi': 'bar'}},
        }
        with patch('opentf.toolkit.core.publish_providerresult'), patch(
            'opentf.toolkit.core.publish_error', mock_publish_error
        ):
            toolkit._dispatch_providercommand(mock_plugin, mock_handler, body)
        mock_publish_error.assert_not_called()
        mock_handler.assert_called_once_with({'abc-def': 'foo', 'ghi': 'bar'})

    def test_dispatch_providercommand_with_ok_default(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {
                    ('p', 'c', 'v'): (
                        {
                            'abc-def': {},
                            'ghi': {},
                            'jkl': {'default': 'JkL'},
                        },
                        False,
                    )
                },
                toolkit.OUTPUTS_KEY: {('p', 'c', 'v'): {'foo': 'fOo'}},
            }
        }
        mock_handler = MagicMock()
        mock_handler.__name__ = 'mock_handler'
        body = {
            'metadata': PCV_METADATA_LABELS,
            'step': {'with': {'abc_def': 'foo', 'ghi': 'bar'}},
        }
        with patch('opentf.toolkit.core.publish_providerresult') as mock_ppr, patch(
            'opentf.toolkit.core.publish_error'
        ) as mock_publish_error:
            toolkit._dispatch_providercommand(mock_plugin, mock_handler, body)
        mock_publish_error.assert_not_called()
        mock_ppr.assert_called_once()
        mock_handler.assert_called_once_with(
            {'abc-def': 'foo', 'ghi': 'bar', 'jkl': 'JkL'}
        )

    def test_dispatch_providercommand_with_ok_additional(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {
                    ('p', 'c', 'v'): (
                        {
                            'abc-def': {},
                            'ghi': {},
                            'jkl': {'default': 'JkL'},
                        },
                        True,
                    )
                },
                toolkit.OUTPUTS_KEY: {('p', 'c', 'v'): {'foo': 'fOo'}},
            }
        }
        mock_handler = MagicMock()
        mock_handler.__name__ = 'mock_handler'
        body = {
            'metadata': PCV_METADATA_LABELS,
            'step': {'with': {'abc_def': 'foo', 'ghi': 'bar', 'xyz': 'oHo'}},
        }
        with patch('opentf.toolkit.core.publish_providerresult'), patch(
            'opentf.toolkit.core.publish_error'
        ) as mock_publish_error:
            toolkit._dispatch_providercommand(mock_plugin, mock_handler, body)
        mock_publish_error.assert_not_called()
        mock_handler.assert_called_once_with(
            {'abc-def': 'foo', 'ghi': 'bar', 'jkl': 'JkL', 'xyz': 'oHo'}
        )

    def test_dispatch_providercommand_with_unexpected_additional(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {
                    ('p', 'c', 'v'): (
                        {
                            'abc-def': {},
                            'ghi': {},
                            'jkl': {'default': 'JkL'},
                        },
                        False,
                    )
                },
                toolkit.OUTPUTS_KEY: {('p', 'c', 'v'): {'foo': 'fOo'}},
            }
        }
        mock_handler = MagicMock()
        mock_handler.__name__ = 'mock_handler'
        body = {
            'metadata': PCV_METADATA_LABELS,
            'step': {'with': {'abc_def': 'foo', 'ghi': 'bar', 'xyz': 'oHo'}},
        }
        with patch('opentf.toolkit.core.publish_providerresult'), patch(
            'opentf.toolkit.core.publish_error'
        ) as mock_publish_error:
            toolkit._dispatch_providercommand(mock_plugin, mock_handler, body)
        mock_publish_error.assert_called_with(
            "Unexpected input 'xyz' found in function step.  Allowed inputs: 'abc-def', 'ghi', 'jkl'."
        )
        mock_handler.assert_not_called()

    def test_dispatch_providercommand_with_missing_required(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {
                    ('p', 'c', 'v'): (
                        {
                            'abc-def': {},
                            'ghi': {'required': True},
                            'jkl': {'default': 'JkL'},
                        },
                        False,
                    )
                }
            }
        }
        mock_publish_error = MagicMock()
        mock_handler = MagicMock()
        mock_handler.__name__ = 'mock_handler'
        body = {
            'metadata': PCV_METADATA_LABELS,
            'step': {'with': {'abc_def': 'foo'}},
        }
        with patch('opentf.toolkit.core.publish_providerresult'), patch(
            'opentf.toolkit.core.publish_error', mock_publish_error
        ):
            toolkit._dispatch_providercommand(mock_plugin, mock_handler, body)
        mock_publish_error.assert_called_with("Mandatory input 'ghi' not provided.")
        mock_handler.assert_not_called()

    def test_dispatch_providercommand_with_nok_normalize(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {
                    ('p', 'c', 'v'): (
                        {
                            'abc-def': {},
                            'ghi': {'required': True},
                            'jkl': {'default': 'JkL'},
                        },
                        False,
                    )
                }
            }
        }
        mock_publish_error = MagicMock()
        mock_handler = MagicMock()
        mock_handler.__name__ = 'mock_handler'
        body = {
            'metadata': PCV_METADATA_LABELS,
            'step': {'with': {'abc_def': 'foo', 'abc-def': 'bad', 'ghi': 'bar'}},
        }
        with patch('opentf.toolkit.core.publish_providerresult'), patch(
            'opentf.toolkit.core.publish_error', mock_publish_error
        ):
            toolkit._dispatch_providercommand(mock_plugin, mock_handler, body)
        mock_publish_error.assert_called_once_with(
            "Both 'abc_def' and 'abc-def' specified in inputs."
        )
        mock_handler.assert_not_called()

    # _ensure_inputs_match

    def test_ensure_inputs_pattern(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {
                toolkit.INPUTS_KEY: {('a', 'a', None): ({'{pattern}': {}}, True)}
            }
        }
        labels = {
            'opentestfactory.org/categoryPrefix': 'a',
            'opentestfactory.org/category': 'a',
        }
        inputs = {'abC-deF': 12, 'ab_de$': 34}
        toolkit._ensure_inputs_match(mock_plugin, labels, inputs)
        self.assertEqual(len(inputs), 2)
        self.assertIn('abC-deF', inputs)
        self.assertIn('ab_de$', inputs)

    # _dispatch_executioncommand

    def test_dispatch_executioncommand_ok(self):
        with patch('opentf.toolkit.core.publish_error') as mock_pe:
            self.assertIsNone(toolkit._dispatch_executioncommand(None, lambda x: x, {}))
        mock_pe.assert_not_called()

    def test_dispatch_executioncommand_raise(self):
        mock_handler = MagicMock(side_effect=Exception('oOops'))
        mock_handler.__name__ = 'moCkHandlER'
        mock_plugin = MagicMock()
        with patch('opentf.toolkit.core.publish_error') as mock_pe:
            what = toolkit._dispatch_executioncommand(mock_plugin, mock_handler, {})
        mock_pe.assert_called_once_with('Unexpected execution error: oOops.')

    # _dispatch_generatorcommand

    def test_dispatch_generatorcommand_ok(self):
        mock_plugin = MagicMock()
        body = {'metadata': {}, 'with': {'some': 'items'}}
        with patch(
            'opentf.toolkit.core.publish_generatorresult'
        ) as mock_publish, patch('opentf.toolkit._ensure_inputs_match') as mock_eim:
            toolkit._dispatch_generatorcommand(mock_plugin, lambda x: x, body)
        mock_eim.assert_called_once()
        mock_publish.assert_called_once()
        args = mock_publish.call_args[0]
        self.assertEqual(args[0], {'some': 'items'})

    def test_dispatch_generatorcommand_raise(self):
        mock_handler = MagicMock(side_effect=Exception('oOops'))
        mock_handler.__name__ = 'moCkHandlER'
        mock_plugin = MagicMock()
        with patch('opentf.toolkit.core.publish_error') as mock_pe, patch(
            'opentf.toolkit._ensure_inputs_match'
        ) as mock_eim:
            toolkit._dispatch_generatorcommand(
                mock_plugin, mock_handler, {'metadata': {}}
            )
        mock_eim.assert_called_once()
        mock_pe.assert_called_once_with('Unexpected execution error: oOops.')

    def test_dispatch_generatorcommand_invalidinputs(self):
        mock_handler = MagicMock()
        mock_handler.__name__ = 'moCkHandlER'
        mock_eim = MagicMock(side_effect=Exception('oOops'))
        mock_plugin = MagicMock()
        with patch('opentf.toolkit.core.publish_error') as mock_pe, patch(
            'opentf.toolkit._ensure_inputs_match', mock_eim
        ):
            toolkit._dispatch_generatorcommand(
                mock_plugin, mock_handler, {'metadata': {}}
            )
        mock_eim.assert_called_once()
        mock_pe.assert_called_once_with('Unexpected execution error: oOops.')
        mock_handler.assert_not_called()

    # watch_file

    def test_watch_file_first(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {}
        mock_initializer = MagicMock()
        with patch('opentf.toolkit._start_watchdog', mock_initializer):
            toolkit.watch_file(mock_plugin, 'aFilePath', 'aHandler', 'aSchemaName')
        self.assertIn(toolkit.WATCHEDFILES_KEY, mock_plugin.config)
        self.assertEqual(len(mock_plugin.config[toolkit.WATCHEDFILES_KEY]), 1)
        self.assertEqual(
            mock_plugin.config[toolkit.WATCHEDFILES_KEY]['aFilePath'],
            [('aHandler', ('aSchemaName',), {})],
        )
        mock_initializer.assert_called_once_with(mock_plugin)

    def test_watch_file_notfirst(self):
        mock_event = MagicMock()
        mock_event.set = MagicMock()
        mock_plugin = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: defaultdict(list),
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_plugin.config[toolkit.WATCHEDFILES_KEY]['foo'].append(('aaa', 'bbb'))
        mock_initializer = MagicMock()
        with patch('opentf.toolkit._start_watchdog', mock_initializer):
            toolkit.watch_file(mock_plugin, 'aFilePath', 'aHandler', 'aSchemaName')
        self.assertEqual(len(mock_plugin.config[toolkit.WATCHEDFILES_KEY]), 2)
        self.assertIn('aFilePath', mock_plugin.config[toolkit.WATCHEDFILES_KEY])
        self.assertEqual(
            mock_plugin.config[toolkit.WATCHEDFILES_KEY]['aFilePath'],
            [('aHandler', ('aSchemaName',), {})],
        )
        mock_initializer.assert_not_called()
        mock_event.set.assert_called_once()

    def test_watch_file_doublenotfirst(self):
        mock_plugin = MagicMock()
        mock_watchedfiles = defaultdict(list)
        mock_watchedfiles['foo'].append(('aaa', 'bbb'))
        mock_watchedfiles['aFilePath'].append(('aHandler', ('aSchemaName',), {}))
        mock_event = MagicMock()
        mock_event.set = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: mock_watchedfiles,
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_initializer = MagicMock()
        with patch('opentf.toolkit._start_watchdog', mock_initializer):
            toolkit.watch_file(mock_plugin, 'aFilePath', 'aHandler2', 'aSchemaName2')
        self.assertEqual(len(mock_watchedfiles), 2)
        self.assertIn('aFilePath', mock_watchedfiles)
        self.assertEqual(len(mock_watchedfiles['aFilePath']), 2)
        self.assertIn(
            ('aHandler', ('aSchemaName',), {}), mock_watchedfiles['aFilePath']
        )
        self.assertIn(
            ('aHandler2', ('aSchemaName2',), {}), mock_watchedfiles['aFilePath']
        )
        mock_initializer.assert_not_called()
        mock_event.set.assert_called_once()

    # _watchnotifier

    def test_watchnotifier_dummy(self):
        mock_plugin = MagicMock()
        mock_plugin.logger = MagicMock()
        mock_plugin.logger.debug = MagicMock(side_effect=Exception('foo'))
        mock_notify = MagicMock(side_effect=[None, Exception('bar')])
        mock_check = MagicMock(side_effect=[1, 2, 3, 4, 5, 6])
        self.assertRaisesRegex(
            Exception,
            'foo',
            toolkit._watchnotifier,
            mock_plugin,
            0,
            mock_check,
            [1, 2],
            mock_notify,
        )
        mock_notify.assert_called()
        mock_plugin.logger.debug.assert_called_once_with(
            'Unexpected exception in watchnotifier, ignoring... bar'
        )

    # _watchdog

    def test_watchdog_nothing_to_do(self):
        mock_event = MagicMock()
        mock_event.wait = MagicMock(side_effect=Exception('timeout'))
        mock_plugin = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: defaultdict(list),
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        self.assertRaises(Exception, toolkit._watchdog, mock_plugin, 0)
        mock_event.wait.assert_called_once()

    def test_watchdog_cannot_stat_onfirstcall(self):
        file_to_watch = 'aFile'
        mock_handler = MagicMock()
        mock_plugin = MagicMock()
        mock_event = MagicMock()
        mock_event.wait = MagicMock(side_effect=Exception('timeout'))
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: {file_to_watch: [(mock_handler, (), {})]},
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_stat = MagicMock(side_effect=OSError('cannot stat'))
        with patch('opentf.toolkit.os.stat', mock_stat):
            self.assertRaises(Exception, toolkit._watchdog, mock_plugin, 0)
        mock_event.wait.assert_called_once()
        mock_stat.assert_called_once_with(file_to_watch)
        mock_handler.assert_called_once()

    def test_watchdog_cannot_stat_onsecondcall(self):
        file_to_watch = 'aFile'
        mock_handler = MagicMock()
        mock_event = MagicMock()
        mock_event.wait = MagicMock()
        mock_event.wait.side_effect = [None, Exception('timeout')]
        mock_plugin = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: {file_to_watch: [(mock_handler, (), {})]},
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 123
        mock_stat = MagicMock()
        mock_stat.side_effect = [mock_stat_result, OSError('cannot stat')]
        with patch('opentf.toolkit.os.stat', mock_stat):
            self.assertRaises(Exception, toolkit._watchdog, mock_plugin, 0)
        self.assertEqual(mock_event.wait.call_count, 2)
        mock_stat.assert_called_with(file_to_watch)
        self.assertEqual(mock_handler.call_count, 2)

    def test_watchdog_modified_handler_ok(self):
        file_to_watch = 'aFile'
        mock_handler = MagicMock()
        mock_event = MagicMock()
        mock_event.wait = MagicMock(side_effect=Exception('timeout'))
        mock_plugin = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: {file_to_watch: [(mock_handler, (), {})]},
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 456
        mock_stat = MagicMock(return_value=mock_stat_result)
        with patch('opentf.toolkit.os.stat', mock_stat):
            self.assertRaises(Exception, toolkit._watchdog, mock_plugin, 0)
        mock_event.wait.assert_called_once()
        mock_stat.assert_called_once_with(file_to_watch)
        mock_handler.assert_called_once_with(mock_plugin, file_to_watch)

    def test_watchdog_notmodified_handler_notcalled(self):
        file_to_watch = 'aFile'
        mock_handler = MagicMock()
        mock_event = MagicMock()
        mock_event.wait = MagicMock(side_effect=Exception('timeout'))
        mock_plugin = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: {file_to_watch: [(mock_handler, ('foo',), {})]},
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 0
        mock_stat = MagicMock(return_value=mock_stat_result)
        with patch('opentf.toolkit.os.stat', mock_stat):
            self.assertRaises(Exception, toolkit._watchdog, mock_plugin, 0)
        mock_event.wait.assert_called_once()
        mock_stat.assert_called_once_with(file_to_watch)
        mock_handler.assert_called_once()

    def test_watchdog_modified_handlers_okko(self):
        file_to_watch = 'aFile'
        mock_handler_ok = MagicMock()
        mock_handler_nok = MagicMock(side_effect=Exception('ooOps'))
        mock_event = MagicMock()
        mock_event.wait = MagicMock(side_effect=Exception('timeout'))
        mock_plugin = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: {
                file_to_watch: [
                    (mock_handler_ok, ('foo',), {}),
                    (mock_handler_nok, (), {'bar': 'baz'}),
                ]
            },
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 456
        mock_stat = MagicMock(return_value=mock_stat_result)
        with patch('opentf.toolkit.os.stat', mock_stat):
            self.assertRaises(Exception, toolkit._watchdog, mock_plugin, 0)
        mock_event.wait.assert_called_once()
        mock_stat.assert_called_once_with(file_to_watch)
        mock_handler_ok.assert_called_once_with(mock_plugin, file_to_watch, 'foo')
        mock_handler_nok.assert_called_once_with(mock_plugin, file_to_watch, bar='baz')

    def test_watchdog_modified_handlers_kook(self):
        file_to_watch = 'aFile'
        mock_handler_ok = MagicMock()
        mock_handler_nok = MagicMock(side_effect=Exception('ooOps'))
        mock_event = MagicMock()
        mock_event.wait = MagicMock(side_effect=Exception('timeout'))
        mock_plugin = MagicMock()
        mock_plugin.config = {
            toolkit.WATCHEDFILES_KEY: {
                file_to_watch: [
                    (mock_handler_nok, (), {}),
                    (mock_handler_ok, ('foo',), {'bar': 'baz'}),
                ]
            },
            toolkit.WATCHEDFILES_EVENT_KEY: mock_event,
        }
        mock_stat_result = MagicMock()
        mock_stat_result.st_mtime = 456
        mock_stat = MagicMock(return_value=mock_stat_result)
        with patch('opentf.toolkit.os.stat', mock_stat):
            self.assertRaises(Exception, toolkit._watchdog, mock_plugin, 0)
        mock_event.wait.assert_called_once()
        mock_stat.assert_called_once_with(file_to_watch)
        mock_handler_ok.assert_called_once_with(
            mock_plugin, file_to_watch, 'foo', bar='baz'
        )
        mock_handler_nok.assert_called_once_with(mock_plugin, file_to_watch)

    # _start_watchdog

    def test_start_watchdog_default_delay(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {'CONTEXT': {}, 'DESCRIPTOR': []}
        mock_watchdog = MagicMock()
        with patch('opentf.toolkit._watchdog', mock_watchdog):
            toolkit._start_watchdog(mock_plugin)
        mock_watchdog.assert_called_once_with(
            mock_plugin, toolkit.WATCHDOG_POLLING_DELAY_SECONDS
        )

    def test_start_watchdog_invalid_delay(self):
        mock_plugin = MagicMock()
        mock_plugin.name = 'myplugin'
        mock_plugin.config = {
            'CONTEXT': {toolkit.WATCHDOG_POLLING_DELAY_KEY: 'oh no'},
            'DESCRIPTOR': [],
        }
        with patch('opentf.toolkit._watchdog') as mock_watchdog:
            self.assertRaises(SystemExit, toolkit._start_watchdog, mock_plugin)
        mock_watchdog.assert_not_called()

    def test_start_watchdog_custom_delay(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {toolkit.WATCHDOG_POLLING_DELAY_KEY: 999},
            'DESCRIPTOR': [],
        }
        mock_watchdog = MagicMock()
        with patch('opentf.toolkit._watchdog', mock_watchdog):
            toolkit._start_watchdog(mock_plugin)
        mock_watchdog.assert_called_once_with(mock_plugin, 999)

    def test_start_watchdog_minimum_delay(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONTEXT': {toolkit.WATCHDOG_POLLING_DELAY_KEY: 9},
            'DESCRIPTOR': [],
        }
        mock_watchdog = MagicMock()
        with patch('opentf.toolkit._watchdog', mock_watchdog):
            toolkit._start_watchdog(mock_plugin)
        mock_watchdog.assert_called_once_with(
            mock_plugin, toolkit.WATCHDOG_POLLING_DELAY_SECONDS
        )
