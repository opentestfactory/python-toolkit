# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Expressions unit tests."""

import logging
import unittest

from unittest.mock import MagicMock, patch

from opentf.toolkit import core


class TestCore(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # publish_providerresult

    def test_ppr_nohooks(self):
        mock_pe = MagicMock()
        mock_config = MagicMock()
        mock_config.config = {'CONFIG': {}}
        mock_gp = MagicMock(return_value=mock_config)
        mock_gb = MagicMock(return_value={'metadata': {'yadA': 'fOO'}})
        with patch('opentf.toolkit.core.publish_event', mock_pe), patch(
            'opentf.toolkit.core._getplugin', mock_gp
        ), patch('opentf.toolkit.core._getbody', mock_gb):
            core.publish_providerresult([{str(x): x} for x in range(10)], None)
        mock_pe.assert_called_once()
        event = mock_pe.call_args[0][0]
        self.assertIn('metadata', event)
        self.assertIn('steps', event)
        self.assertNotIn('hooks', event)
        self.assertNotIn('outputs', event)
        self.assertEqual(len(event['steps']), 10)

    def test_ppr_hooks(self):
        mock_pe = MagicMock()
        mock_config = MagicMock()
        mock_config.config = {'CONFIG': {'hooks': 'hOOkS'}}
        mock_gp = MagicMock(return_value=mock_config)
        mock_gb = MagicMock(return_value={'metadata': {'yadA': 'fOO'}})
        with patch('opentf.toolkit.core.publish_event', mock_pe), patch(
            'opentf.toolkit.core._getplugin', mock_gp
        ), patch('opentf.toolkit.core._getbody', mock_gb):
            core.publish_providerresult([{str(x): x} for x in range(10)], {})
        mock_pe.assert_called_once()
        event = mock_pe.call_args[0][0]
        self.assertIn('metadata', event)
        self.assertIn('steps', event)
        self.assertIn('hooks', event)
        self.assertNotIn('outputs', event)
        self.assertEqual(len(event['steps']), 10)
        self.assertIn('id', event['steps'][0])

    # pubish_event

    def test_publishevent_ok(self):
        mock_app = MagicMock()
        mock_app.config = {'__dispatch queue__': MagicMock()}
        mock_gp = MagicMock(return_value=mock_app)
        with patch('opentf.toolkit.core._getplugin', mock_gp):
            core.publish_event({})
        mock_app.config['__dispatch queue__'].put.assert_called_once_with({})

    # publish_generatorresult

    def test_publish_generatorresult_ok(self):
        mock_pe = MagicMock()
        mock_gb = MagicMock()
        with patch('opentf.toolkit.core.publish_event', mock_pe), patch(
            'opentf.toolkit.core._getbody', mock_gb
        ):
            core.publish_generatorresult({'job1': {'a': 'b'}, 'job2': {'c': 'd'}}, None)
        mock_pe.assert_called_once()
        event = mock_pe.call_args[0][0]
        self.assertEqual('GeneratorResult', event['kind'])
        self.assertEqual(2, len(event['jobs']))
        self.assertEqual({'a': 'b'}, event['jobs']['job1'])
        self.assertNotIn('outputs', event)

    def test_normalize_path_1(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.normalize_path('foo/bar'), 'foo/bar')

    def test_normalize_path_2(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.normalize_path('foo/bar baz'), '"foo/bar baz"')

    def test_normalize_path_3(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.normalize_path('foo/bar baz'), '"foo\\bar baz"')

    def test_secret_1(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.set_secret('foo'), 'echo "::add-mask::foo"')

    def test_secret_2(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.set_secret('foo'), '@echo ::add-mask::foo')

    def test_export_variable_1(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.export_variable('foo', 'bar'),
                'echo export foo=bar >> "$OPENTF_VARIABLES"',
            )

    def test_export_variable_2(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.export_variable('foo', 'bar baz'),
                'echo export foo=\\"bar baz\\" >> "$OPENTF_VARIABLES"',
            )

    def test_export_variable_3(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.export_variable('foo', 'bar'),
                '@echo set "foo=bar" >>"%OPENTF_VARIABLES%"',
            )

    def test_export_variable_verbatim_linux(self):
        with patch(
            'opentf.toolkit.core.runner_on_windows', MagicMock(return_value=False)
        ):
            result = core.export_variable('var', '@|val', True)
        self.assertIn("export var='$(echo QHx2YWw= | base64 --decode)'", result)

    def test_export_variable_verbatim_windows(self):
        mock_uuid = MagicMock(side_effect=['uuid1', 'uuid2'])
        expected = 'echo @echo QHx2YWwj ^> uuid1 ^& @certutil -f -decode uuid1 uuid2 ^>nul ^& @del uuid1 ^& set /P var^=^<uuid2 ^& del uuid2'
        with patch(
            'opentf.toolkit.core.runner_on_windows', MagicMock(return_value=True)
        ), patch('opentf.toolkit.core.make_uuid', mock_uuid):
            result = core.export_variable('var', '@|val#', True)
        self.assertIn(expected, result)

    def test_attach_file_1(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.attach_file('foo/bar baz', bar='BaR'),
                'echo "::attach bar=BaR::`pwd`/foo/bar baz"',
            )

    def test_attach_file_2(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.attach_file('foo'),
                'echo "::attach::`pwd`/foo"',
            )

    def test_attach_file_3(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.attach_file('foo/bar'),
                '@echo ::attach::%CD%\\foo\\bar',
            )

    def test_delete_file_1(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.delete_file('foo'), 'rm -f foo')

    def test_delete_file_2(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.delete_file('foo/bar'), '@if exist foo\\bar @del /f/q foo\\bar'
            )

    def test_delete_directory_1(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.delete_directory('foo'), 'rm -rf foo')

    def test_delete_directory_2(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(
                core.delete_directory('foo/bar'),
                '@if exist foo\\bar @rmdir /s/q foo\\bar',
            )

    def test_touch_file_1(self):
        mock = MagicMock(return_value=False)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.touch_file('foo'), 'touch foo')

    def test_touch_file_2(self):
        mock = MagicMock(return_value=True)
        with patch('opentf.toolkit.core.runner_on_windows', mock):
            self.assertEqual(core.touch_file('foo/bar'), '@type nul >>foo\\bar')

    def test_join_path_1(self):
        self.assertEqual(core.join_path('foo', None, False), 'foo')

    def test_join_path_2(self):
        self.assertEqual(core.join_path('foo/bar', 'baz', False), 'foo/bar/baz')

    def test_join_path_3(self):
        self.assertEqual(core.join_path('foo/bar', 'baz', True), 'foo\\bar\\baz')

    def test_join_path_4(self):
        self.assertEqual(core.join_path('foo/bar', None, True), 'foo\\bar')

    def test_join_path_5(self):
        self.assertEqual(core.join_path('foo/bar', '/baz', True), '\\baz')

    def test_join_path_6(self):
        self.assertEqual(core.join_path('foo/bar', '/baz', False), '/baz')

    def test_fail(self):
        self.assertRaises(core.ExecutionError, core.fail, 'Error DescRipTIOn ')

    def test_register_defaultplugin_1(self):
        core.register_defaultplugin('fOo')
        self.assertEqual(core.DEFAULT_PLUGIN, 'fOo')

    def test_deregister_defaultplugin_1(self):
        core.register_defaultplugin('fOo')
        core.deregister_defaultplugin('fOo')
        self.assertIsNone(core.DEFAULT_PLUGIN)

    def test_deregister_defaultplugin_2(self):
        core.register_defaultplugin('fOo')
        core.deregister_defaultplugin('bAr')
        self.assertIsNotNone(core.DEFAULT_PLUGIN)

    def test_export_variables(self):
        data_1 = {'key1': 'value1'}
        data_2 = {'key2': 'value2'}
        mock_export = MagicMock(return_value='foobar')
        with patch('opentf.toolkit.core.export_variable', mock_export):
            steps = core.export_variables(data_1, data_2, verbatim=True)
        self.assertEqual(2, mock_export.call_count)
        self.assertEqual(1, len(steps))
        self.assertEqual(('key1', 'value1', True), mock_export.call_args_list[0][0])
        self.assertEqual(('key2', 'value2', True), mock_export.call_args_list[1][0])

    def test_validate_params_inputs_ok(self):
        inputs = {
            'format': core.SQUASHTM_FORMAT,
            'data': {'global': {'var1': 'value1'}, 'test': {'var2': 'value2'}},
        }
        mock_fail = MagicMock()
        with patch('opentf.toolkit.core.fail', mock_fail):
            core.validate_params_inputs(inputs)
        mock_fail.assert_not_called()

    def test_validate_params_inputs_ko_no_squash_format(self):
        inputs = {
            'format': 'whiii',
            'data': {'global': {'var1': 'value1'}, 'test': {'var2': 'value2'}},
        }
        mock_fail = MagicMock()
        with patch('opentf.toolkit.core.fail', mock_fail):
            core.validate_params_inputs(inputs)
        mock_fail.assert_called_once_with('Unknown format value for params.')

    def test_validate_params_inputs_ko_unexpected_keys(self):
        inputs = {
            'format': core.SQUASHTM_FORMAT,
            'data': {'not_global': {'var1': 'value1'}, 'not_test': {'var2': 'value2'}},
        }
        mock_fail = MagicMock()
        with patch('opentf.toolkit.core.fail', mock_fail):
            core.validate_params_inputs(inputs)
        mock_fail.assert_called_once_with(
            'Unexpected keys found in data, was only expecting global and/or test.'
        )
